package com.waleed.KnightsAndArrows;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.waleed.KnightsAndArrows.util.Downloader;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;

public class UpdateScreen implements com.badlogic.gdx.Screen
{
  private Stage stage1;
  private Stage stage2;
  private Stage stage3;
  private Stage stage4;
  private Skin skin;
  private TextureAtlas atlas;
  private SpriteBatch batch;
  private com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable mapSelectTexture;
  public byte state = 0;
  
  public boolean error = false;
  
  public boolean isCancelled = false;
  
  public static String errorMsg;
  
  Downloader downloader;
  Thread downloaderThread;
  public boolean finished;
  public boolean downloadError = false;
  
  public void render(float delta) {
    Gdx.gl.glClear(16384);
    Gdx.gl.glClearColor(0.0F, 0.0F, 0.0F, 0.0F);
    switch (this.state) {
    case 0: 
      Gdx.input.setInputProcessor(this.stage1);
      this.stage1.act(delta);
      this.stage1.draw();
      this.batch.begin();
      short w = 1024;
      short h = 384;
      BitmapFont.TextBounds tb = Resources.FONT.getBounds("Would you like to update?");
      Resources.FONT.setScale(3.0F);
      
      Resources.FONT.draw(this.batch, "Would you like to update?", Gdx.graphics.getWidth() / 2 - tb.width / 2.0F - 100.0F, Gdx.graphics.getHeight() / 2 + tb.height + 50.0F);
      Resources.FONT.setScale(2.0F);
      this.batch.end();
      break;
    case 1: 
      Gdx.input.setInputProcessor(this.stage2);
      this.stage2.act(delta);
      this.stage2.draw();
      this.batch.begin();
      
      if (this.downloader.isError)
      {
        this.state = 2;
      } else if (this.downloader.isFinished)
      {
        this.state = 3;
      }
      else {
        BitmapFont.TextBounds tb2 = Resources.FONT.getBounds("Downloading...");
        Resources.FONT.setScale(2.0F);
        Resources.FONT.draw(this.batch, "Downloading...", Gdx.graphics.getWidth() / 2 - tb2.width / 2.0F, Gdx.graphics.getHeight() / 2 + tb2.height);
      }
      this.batch.end();
      
      break;
    case 2: 
      Gdx.input.setInputProcessor(this.stage4);
      this.stage4.act(delta);
      this.stage4.draw();
      this.batch.begin();
      BitmapFont.TextBounds tb3 = Resources.FONT.getBounds(getError());
      Resources.FONT.setScale(2.0F);
      Resources.FONT.draw(this.batch, getError(), Gdx.graphics.getWidth() / 2 / 2.0F - 50.0F, Gdx.graphics.getHeight() / 2 + tb3.height);
      Resources.FONT.setScale(0.5F);
      Resources.FONT.setColor(Color.RED);
      this.batch.end();
      break;
    
    case 3: 
      Gdx.input.setInputProcessor(this.stage3);
      this.stage3.act(delta);
      this.stage3.draw();
      this.batch.begin();
      BitmapFont.TextBounds tb4 = Resources.FONT.getBounds("Finished downloading! Press OK to restart the game");
      Resources.FONT.setScale(2.0F);
      Resources.FONT.draw(this.batch, "Finished downloading! Press OK to restart the game", Gdx.graphics.getWidth() / 2 / 2.0F - 50.0F, Gdx.graphics.getHeight() / 2 + tb4.height);
      Resources.FONT.setScale(0.5F);
      Resources.FONT.setColor(Color.GREEN);
      this.batch.end();
      break;
    
    default: 
      this.state = 0;
    }
    
  }
  













  private static boolean isDownloadAvaliable()
  {
    try
    {
      URL url = new URL(KnightsAndArrows.INSTANCE.downloadURL);
      java.net.URLConnection conn = url.openConnection();
      conn.connect();
      System.out.println("Download Server is stable");
      return true;
    } catch (MalformedURLException e) {
      String error = "Download Server is unstable" + e.toString();
      System.out.println(error);
      setError(error);
      return false;
    } catch (IOException e) {
      String error = "Download Server is unstable: " + e.toString();
      System.out.println(error);
      setError(error); }
    return false;
  }
  


  public static void setError(String error)
  {
    errorMsg = error;
  }
  
  public static String getError()
  {
    return errorMsg;
  }
  
  public void beginDownload(boolean isCancelled) throws InterruptedException
  {
    if (isDownloadAvaliable())
    {
      this.downloader = new Downloader();
      boolean error = this.downloader.isError();
      this.error = error;
      if (isCancelled)
      {

        this.downloaderThread = new Thread(this.downloader);
        this.downloaderThread.start();
      }
      else
      {
        this.downloaderThread = new Thread(this.downloader);
        this.downloaderThread.start();
      }
    }
    else
    {
      this.state = 2;
    }
  }
  
  public boolean isError()
  {
    return this.error;
  }
  
  public void resize(int width, int height) {
    this.stage1 = new Stage(width, height, true, this.batch);
    this.stage2 = new Stage(width, height, true, this.batch);
    TextButton.TextButtonStyle style = new TextButton.TextButtonStyle();
    style.up = this.skin.getDrawable("button_off");
    style.over = this.skin.getDrawable("button_on");
    style.down = this.skin.getDrawable("button_active");
    style.font = Resources.FONT;
    style.font.setScale(2.0F);
    TextButton startButton = new TextButton("Yes", style);
    startButton.setSize(256.0F, 96.0F);
    startButton.setPosition(Gdx.graphics.getWidth() / 2 - 256, Gdx.graphics.getHeight() / 2 - 160);
    startButton.addListener(new InputListener()
    {
      public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) { return true; }
      
      public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        if (button == 0) {
          Resources.GUISOUNDS[0].play(KnightsAndArrows.VOLUME);
          UpdateScreen.this.state = 1;
          try {
            UpdateScreen.this.beginDownload(UpdateScreen.this.isCancelled);
          }
          catch (InterruptedException e) {
            e.printStackTrace();
          }
        }
        
        super.touchUp(event, x, y, pointer, button);
      }
    });
    this.stage1.addActor(startButton);
    TextButton exitButton = new TextButton("No", style);
    exitButton.setSize(256.0F, 96.0F);
    exitButton.setPosition(Gdx.graphics.getWidth() / 2 + 60, Gdx.graphics.getHeight() / 2 - 160);
    exitButton.addListener(new InputListener()
    {
      public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) { return true; }
      
      public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        if (button == 0) {
          Resources.GUISOUNDS[0].play(KnightsAndArrows.VOLUME);
          if (KnightsAndArrows.isOffline()) {
            KnightsAndArrows.INSTANCE.setScreen(new MenuScreen("Guest", false));
          } else {
            KnightsAndArrows.INSTANCE.setScreen(new MenuScreen(SplashScreen.username, true));
          }
        }
        super.touchUp(event, x, y, pointer, button);
      }
    });
    this.stage1.addActor(exitButton);
    
    TextButton cancelButton = new TextButton("Cancel", style);
    cancelButton.setSize(256.0F, 96.0F);
    cancelButton.setPosition(Gdx.graphics.getWidth() / 2 - 128, Gdx.graphics.getHeight() / 2 - 272);
    cancelButton.addListener(new InputListener()
    {
      public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) { return true; }
      
      public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        if (button == 0) {
          Resources.GUISOUNDS[0].play(KnightsAndArrows.VOLUME);
          UpdateScreen.this.state = 0;
          UpdateScreen.this.isCancelled = true;
        }
        


        super.touchUp(event, x, y, pointer, button);
      }
    });
    this.stage2.addActor(cancelButton);
    

    TextButton backButton = new TextButton("OK", style);
    backButton.setSize(256.0F, 96.0F);
    backButton.setPosition(Gdx.graphics.getWidth() / 2 - 128, Gdx.graphics.getHeight() / 2 - 272);
    backButton.addListener(new InputListener()
    {
      public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) { return true; }
      
      public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        if (button == 0) {
          Resources.GUISOUNDS[0].play(KnightsAndArrows.VOLUME);
          Resources.FONT.setColor(Color.WHITE);
        }
        
        UpdateScreen.this.runCommand();
        
        super.touchUp(event, x, y, pointer, button);
      }
    });
    this.stage3.addActor(backButton);
  }
  



  public void runCommand()
  {
    String s = null;
    

    try
    {
      String bin = KnightsAndArrows.class.getProtectionDomain().getCodeSource().getLocation().getPath();
      String decodedPath = "";
      try {
        decodedPath = java.net.URLDecoder.decode(bin, "UTF-8");
      }
      catch (UnsupportedEncodingException e) {
        e.printStackTrace();
      }
      decodedPath = decodedPath.replace(" ", "%20");
      Process p = Runtime.getRuntime().exec("java -jar " + decodedPath + "KA.jar");
      
      BufferedReader stdInput = new BufferedReader(
        new java.io.InputStreamReader(p.getInputStream()));
      
      BufferedReader stdError = new BufferedReader(
        new java.io.InputStreamReader(p.getErrorStream()));
      

      System.out.println("Here is the standard output of the command:\n");
      while ((s = stdInput.readLine()) != null) {
        System.out.println(s);
      }
      

      System.out.println("Here is the standard error of the command (if any):\n");
      while ((s = stdError.readLine()) != null) {
        System.out.println(s);
      }
      System.exit(0);

    }
    catch (IOException e)
    {
      System.out.println("exception happened - here's what I know: ");
      e.printStackTrace();
      System.exit(-1);
    }
  }
  
  public void show()
  {
    this.batch = new SpriteBatch();
    this.stage1 = new Stage();
    this.stage2 = new Stage();
    this.stage3 = new Stage();
    this.stage4 = new Stage();
    this.atlas = new TextureAtlas(Resources.internal("images/buttons.pack"));
    this.skin = new Skin();
    this.skin.addRegions(this.atlas);
  }
  
  public void hide() {
    dispose();
    this.stage1.dispose();
    this.stage2.dispose();
  }
  
  public void pause() {}
  
  public void resume() {}
  
  public void dispose() {
    this.stage1.dispose();
    this.stage2.dispose();
    this.skin.dispose();
    this.batch.dispose();
    this.atlas.dispose();
  }
}
