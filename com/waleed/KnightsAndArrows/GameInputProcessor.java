package com.waleed.KnightsAndArrows;

import com.badlogic.gdx.InputAdapter;




public class GameInputProcessor
  extends InputAdapter
{
  public boolean keyDown(int k)
  {
    if (k == 19) {
      GameKeys.setKey(0, true);
    }
    
    if (k == 21) {
      GameKeys.setKey(1, true);
    }
    
    if (k == 20) {
      GameKeys.setKey(2, true);
    }
    
    if (k == 22) {
      GameKeys.setKey(3, true);
    }
    
    if (k == 66) {
      GameKeys.setKey(4, true);
    }
    
    if (k == 131) {
      GameKeys.setKey(5, true);
    }
    
    if (k == 62) {
      GameKeys.setKey(6, true);
    }
    
    if ((k == 59) || (k == 60)) {
      GameKeys.setKey(7, true);
    }
    
    if (k == 33) {
      GameKeys.setKey(8, true);
    }
    
    return true;
  }
  
  public boolean keyUp(int k) {
    if (k == 19) {
      GameKeys.setKey(0, false);
    }
    
    if (k == 21) {
      GameKeys.setKey(1, false);
    }
    
    if (k == 20) {
      GameKeys.setKey(2, false);
    }
    
    if (k == 22) {
      GameKeys.setKey(3, false);
    }
    
    if (k == 66) {
      GameKeys.setKey(4, false);
    }
    
    if (k == 131) {
      GameKeys.setKey(5, false);
    }
    
    if (k == 62) {
      GameKeys.setKey(6, false);
    }
    
    if ((k == 59) || (k == 60)) {
      GameKeys.setKey(7, false);
    }
    
    if (k == 33) {
      GameKeys.setKey(8, false);
    }
    
    return true;
  }
}
