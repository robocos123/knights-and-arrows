package com.waleed.KnightsAndArrows.core;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool.PooledEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapLayers;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.waleed.KnightsAndArrows.KnightsAndArrows;
import com.waleed.KnightsAndArrows.Resources;
import com.waleed.KnightsAndArrows.core.entities.Campfire;
import com.waleed.KnightsAndArrows.core.entities.Entity;
import com.waleed.KnightsAndArrows.core.entities.Spawner;
import com.waleed.KnightsAndArrows.core.entities.Squad;
import com.waleed.KnightsAndArrows.core.entities.Unit;
import com.waleed.KnightsAndArrows.util.Camera;
import com.waleed.KnightsAndArrows.util.Light;
import com.waleed.KnightsAndArrows.util.LightHandler;
import com.waleed.KnightsAndArrows.util.TwoPassShader;
import java.util.HashMap;
import java.util.Iterator;

public class Level extends TileMap
{
  public ShapeRenderer sr = new ShapeRenderer();
  private Camera camera;
  private TwoPassShader lighting;
  private LightHandler lights;
  private Array entities = new Array();
  private Array squads = new Array();
  private Array spawners = new Array();
  public Campfire campfire;
  public Rectangle squadCreateArea;
  private Base blueBase;
  private Base redBase;
  private float time;
  private float beforeEnd = 1.0F;
  private boolean finished;
  private int stage = 1;
  private float brightness = 1.0F;
  public int killCount;
  private boolean won;
  private boolean timerRunning = true;
  public int gold = 200;
  private long fireID;
  private HashMap pools = new HashMap();
  private Array effects = new Array();
  
  public Level(String tmxPath)
  {
    super(tmxPath);
    this.fireID = Resources.SOUNDS[10].play(0.0F);
    Resources.SOUNDS[10].setLooping(this.fireID, true);
    Resources.SOUNDS[10].setVolume(this.fireID, 0.5F);
    this.lights = new LightHandler(512);
    this.lights.setCasterPass(new com.waleed.KnightsAndArrows.util.ShadowCasterPass() {
      public void render(SpriteBatch batch, Light light) {
        Level.this.renderShadowCasters(batch, light, true);
      }
    });
    this.lighting = new TwoPassShader(Resources.internal("shaders/pass.vs"), Resources.internal("shaders/blend.fs"));
    String[] names = { "fire", "torchfire", "arrowhit", "magichit", "heal", "squaddefeat", "wind", "boom" };
    String[] ty = names;
    int tx = names.length;
    
    for (int first = 0; first < tx; first++) {
      String i = ty[first];
      ParticleEffect fireEffect = new ParticleEffect();
      fireEffect.load(Resources.internal("effects/" + i + ".p"), Resources.internal("effects"));
      this.pools.put(i, new ParticleEffectPool(fireEffect, 1, 64));
    }
    
    loadObjects();
    
    for (int var8 = 0; var8 < 2; var8++) {
      Squad var9 = new Squad(this.camera.position.x, this.camera.position.y, this);
      var9.addRandomMembers(4);
      float var10 = this.squadCreateArea.x + MathUtils.random() * this.squadCreateArea.width;
      float var11 = this.squadCreateArea.y + MathUtils.random() * this.squadCreateArea.height;
      var9.setTarget((int)(var10 / 32.0F), (int)(var11 / 32.0F));
    }
  }
  
  private void loadObjects()
  {
    MapLayer objectLayer = this.map.getLayers().get("objects");
    Iterator actions = objectLayer.getObjects().iterator();
    



    while (actions.hasNext()) {
      MapObject spawnLayer = (MapObject)actions.next();
      String o = spawnLayer.getName();
      int name = Integer.parseInt(spawnLayer.getProperties().get("x").toString()) << 2;
      int name1 = Integer.parseInt(spawnLayer.getProperties().get("y").toString()) << 2;
      int x = Integer.parseInt(spawnLayer.getProperties().get("width").toString()) << 2;
      int y = Integer.parseInt(spawnLayer.getProperties().get("height").toString()) << 2;
      if (o.equals("torch")) {
        add(new com.waleed.KnightsAndArrows.core.entities.Torch(name, name1));
      } else if (o.equals("campfire")) {
        this.campfire = new Campfire(name, name1);
        add(this.campfire);
      } else if (o.equals("blue_base")) {
        this.blueBase = new Base(name + x / 2 - 16, name1 + y / 2 - 16, false);
        add(this.blueBase);
      } else if (o.equals("red_base")) {
        this.redBase = new Base(name + x / 2 - 16, name1 + y / 2 - 16, true);
        add(this.redBase);
      } else if (o.equals("potato")) {
        add(new com.waleed.KnightsAndArrows.core.entities.Potato(name + x / 2, name1 + y / 2));
      }
    }
    
    MapLayer spawnLayer1 = this.map.getLayers().get("spawns");
    Iterator o1 = spawnLayer1.getObjects().iterator();
    
    while (o1.hasNext()) {
      MapObject actions1 = (MapObject)o1.next();
      String name2 = actions1.getName();
      int name1 = Integer.parseInt(actions1.getProperties().get("x").toString()) << 2;
      int x = Integer.parseInt(actions1.getProperties().get("y").toString()) << 2;
      int y = Integer.parseInt(actions1.getProperties().get("width").toString()) << 2;
      int h = Integer.parseInt(actions1.getProperties().get("height").toString()) << 2;
      if (name2.equals("basic")) {
        int delay = Integer.parseInt(actions1.getProperties().get("delay").toString());
        Base target = actions1.getProperties().get("target").toString().equals("blue_base") ? this.blueBase : this.redBase;
        int max = Integer.parseInt(actions1.getProperties().get("max").toString());
        this.spawners.add(new com.waleed.KnightsAndArrows.core.entities.BasicSpawner(name1, x, y, h, delay, max, target));
      } else if (name2.equals("create")) {
        this.squadCreateArea = new Rectangle(name1, x, y, h);
      }
    }
    
    MapLayer actions2 = this.map.getLayers().get("actions");
    Iterator name3 = actions2.getObjects().iterator();
    
    while (name3.hasNext()) {
      MapObject o2 = (MapObject)name3.next();
      String name4 = o2.getName();
      int x = Integer.parseInt(o2.getProperties().get("x").toString()) << 2;
      int y = Integer.parseInt(o2.getProperties().get("y").toString()) << 2;
      if (name4.equals("camera")) {
        this.camera = new Camera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        this.camera.position.set(x, y, 0.0F);
        this.camera.tmpPos.set(x, y);
        this.camera.setTarget(x, y);
        this.camera.update();
      }
    }
  }
  
  public void add(Entity e)
  {
    e.enter(this);
    this.entities.add(e);
    this.entities.sort();
  }
  
  public void add(Light l) {
    this.lights.add(l);
  }
  
  public void remove(Squad s) {
    this.squads.removeValue(s, false);
  }
  
  public void add(String effectName, float x, float y) {
    ParticleEffectPool.PooledEffect effect = ((ParticleEffectPool)this.pools.get(effectName)).obtain();
    effect.setPosition(x, y);
    this.effects.add(effect);
  }
  
  public void remove(Entity e) {
    this.entities.removeValue(e, false);
  }
  
  private void renderShadowCasters(SpriteBatch batch, Light light, boolean renderEntities) {
    if (light.castShadows) {
      Rectangle bounds = new Rectangle(this.mapRenderer.getViewBounds());
      float ls = this.lights.getLightSize() / 2.0F;
      this.mapRenderer.getViewBounds().set(bounds.x - ls, bounds.y - ls, bounds.width + ls * 2.0F, bounds.height + ls * 2.0F);
      renderLayers("ao", batch.getProjectionMatrix());
      this.mapRenderer.getViewBounds().set(bounds);
      if (renderEntities) {
        batch.begin();
        Iterator var7 = this.entities.iterator();
        
        while (var7.hasNext()) {
          Entity e = (Entity)var7.next();
          if (e.getID() != light.mask) {
            e.render(batch);
          }
        }
        
        batch.end();
      }
    }
  }
  
  public void updateAndRender(float delta, Matrix4 screenView, SpriteBatch batch, boolean paused)
  {
    this.sr.setProjectionMatrix(screenView);
    
    if (!paused) {
      if (this.timerRunning) {
        this.time += delta;
      } else {
        this.beforeEnd -= delta;
      }
      

      if (this.beforeEnd <= 0.0F) {
        this.beforeEnd = 3.0F;
        if (this.finished) {
          KnightsAndArrows.INSTANCE.setScreen(new com.waleed.KnightsAndArrows.EndScreen(this.won, this.killCount, this.time, KnightsAndArrows.INSTANCE.username));
          Resources.SOUNDS[10].stop();
        } else {
          this.finished = true;
          Vector2 i = this.won ? this.redBase.getBounds().getCenter() : this.blueBase.getBounds().getCenter();
          Iterator d = this.squads.iterator();
          
          while (d.hasNext()) {
            Squad effect = (Squad)d.next();
            effect.getTargets().clear();
            effect.stopBaseAttack();
          }
          
          Resources.play(7, i.x, i.y, this);
          add("boom", i.x, i.y);
          this.blueBase.remove();
          this.redBase.remove();
        }
      }
      
      float var9 = 0.35F;
      this.brightness = ((MathUtils.sin(this.time * 2.0F) + 1.0F) * ((1.0F - var9) / 2.0F) + var9);
      this.brightness = var9;
      this.stage = ((int)(this.time / 70.0F) + 1);
      Iterator d = this.spawners.iterator();
      
      while (d.hasNext()) {
        Spawner var12 = (Spawner)d.next();
        var12.update(delta, this);
      }
      
      this.lights.update(delta, batch, this.camera, this.brightness);
      float var11 = new Vector2(this.camera.position.x, this.camera.position.y).dst(this.campfire.getBounds().x, this.campfire.getBounds().y);
      float var13 = (float)Math.sqrt(Gdx.graphics.getWidth() * Gdx.graphics.getWidth() + Gdx.graphics.getHeight() * Gdx.graphics.getHeight()) / 2.0F;
      var11 = (var13 - var11) / var13;
      if (var11 < 0.0F) {
        var11 = 0.0F;
      }
      
      float dx = (this.campfire.getBounds().x - this.camera.position.x) / Gdx.graphics.getWidth() / 2.0F;
      var11 = MathUtils.clamp(var11, 0.0F, KnightsAndArrows.VOLUME);
      Resources.SOUNDS[10].setPan(this.fireID, dx, var11 / 4.0F);
    }
    

    this.lighting.beginFirst();
    setView(this.camera);
    renderLayers("render", this.camera.combined);
    this.lighting.endFirst();
    this.lighting.setSecond(this.lights.getLightTexture());
    batch.setProjectionMatrix(screenView);
    this.lighting.renderToScreen(batch, 1.0F - this.brightness);
    batch.setProjectionMatrix(this.camera.combined);
    
    if (!paused) {
      for (int var10 = this.squads.size - 1; var10 >= 0; var10--) {
        Squad effect = (Squad)this.squads.get(var10);
        effect.update(delta);
      }
    }
    
    batch.begin();
    
    for (int var10 = this.entities.size - 1; var10 >= 0; var10--) {
      Entity var14 = (Entity)this.entities.get(var10);
      if (var14.wasRemoved()) {
        this.entities.removeIndex(var10);
      } else {
        if (!paused) {
          var14.update(delta);
        }
        
        if (this.camera.frustum.rectInFrustrum(var14.getBounds())) {
          var14.render(batch);
        }
      }
    }
    
    batch.end();
    batch.setProjectionMatrix(screenView);
    
    for (int var10 = this.squads.size - 1; var10 >= 0; var10--) {
      Squad effect = (Squad)this.squads.get(var10);
      effect.renderView(batch, this.camera);
    }
    
    this.blueBase.renderView(screenView, this.camera, batch);
    this.redBase.renderView(screenView, this.camera, batch);
    batch.setProjectionMatrix(this.camera.combined);
    batch.begin();
    
    for (int var10 = this.effects.size - 1; var10 >= 0; var10--) {
      ParticleEffectPool.PooledEffect var15 = (ParticleEffectPool.PooledEffect)this.effects.get(var10);
      var15.draw(batch, delta);
      if (var15.isComplete()) {
        var15.free();
        this.effects.removeIndex(var10);
      }
    }
    
    batch.end();
  }
  
  public void resize(int width, int height) {
    this.lights.resize(width, height);
    Vector3 prev = this.camera.position.cpy();
    this.camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
    
    this.camera.position.set(prev);
  }
  
  public LightHandler getLights() {
    return this.lights;
  }
  
  public float getBrightness() {
    return this.brightness;
  }
  
  public Array getEntities() {
    return this.entities;
  }
  
  public Array getSquads() {
    return this.squads;
  }
  
  public void dispose() {
    super.dispose();
    Iterator var2 = this.entities.iterator();
    
    while (var2.hasNext()) {
      Entity e = (Entity)var2.next();
      e.dispose();
    }
    
    var2 = this.effects.iterator();
    
    while (var2.hasNext()) {
      ParticleEffectPool.PooledEffect e1 = (ParticleEffectPool.PooledEffect)var2.next();
      e1.free();
    }
    
    this.effects.clear();
    this.squads.clear();
  }
  
  public boolean isSpotTaken(int x, int y) {
    if (super.isBlocked(x, y)) {
      return true;
    }
    Iterator var4 = this.squads.iterator();
    Iterator var6;
    for (; var4.hasNext(); 
        


        var6.hasNext())
    {
      Squad s = (Squad)var4.next();
      var6 = s.getMembers().iterator();
      
      Unit u = (Unit)var6.next();
      if ((u.getGoal() != null) && (x == (int)u.getGoal().x) && (y == (int)u.getGoal().y)) {
        return true;
      }
    }
    

    return false;
  }
  
  public int getStage()
  {
    return this.stage;
  }
  
  public Camera getCamera() {
    return this.camera;
  }
  
  public Base getBlueBase() {
    return this.blueBase;
  }
  
  public Base getRedBase() {
    return this.redBase;
  }
  
  public void end(boolean won) {
    this.won = won;
    this.timerRunning = false;
    Vector2 p = won ? this.redBase.getBounds().getCenter() : this.blueBase.getBounds().getCenter();
    this.camera.setTarget(p.x, p.y);
  }
  
  public int getSquadCount(boolean enemy) {
    int count = 0;
    Iterator var4 = this.squads.iterator();
    
    while (var4.hasNext()) {
      Squad s = (Squad)var4.next();
      if (s.isEnemy() == enemy) {
        count++;
      }
    }
    
    return count;
  }
}
