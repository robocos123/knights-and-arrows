package com.waleed.KnightsAndArrows.core.gui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GLCommon;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton.ImageButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;
import com.waleed.KnightsAndArrows.Resources;
import com.waleed.KnightsAndArrows.core.Base;
import com.waleed.KnightsAndArrows.core.Level;
import com.waleed.KnightsAndArrows.core.entities.Entity;
import com.waleed.KnightsAndArrows.core.entities.Squad;
import com.waleed.KnightsAndArrows.core.entities.Stats;
import com.waleed.KnightsAndArrows.core.entities.Unit;
import com.waleed.KnightsAndArrows.util.Camera;
import java.util.Iterator;

public class GuiHandler
{
  private Level level;
  private ShapeRenderer sr = new ShapeRenderer();
  private Vector2 lastClick = new Vector2();
  private boolean busy;
  private Object selectedObject;
  private boolean aMenu = false;
  private float aStage;
  private boolean cMenu = false;
  private boolean bMenu = false;
  private TextField name;
  private boolean sReady = true;
  private float ox;
  private float oy;
  private Rectangle selection = new Rectangle();
  private byte sMode = 0;
  private boolean selectAll;
  private Stage stage;
  private com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle style;
  private boolean first = true;
  
  public GuiHandler(Level level)
  {
    this.level = level;
  }
  
  public void updateAndRender(float delta, SpriteBatch batch, Camera camera, boolean paused)
  {
    this.sr.setProjectionMatrix(camera.combined);
    Vector2 mouse = com.waleed.KnightsAndArrows.Input.unprojectMouse(camera);
    


    if ((!this.busy) && (!paused))
    {

      if ((com.waleed.KnightsAndArrows.Input.isButtonPressedAndReleased(0)) && (com.waleed.KnightsAndArrows.Input.getMouseX() < Gdx.graphics.getWidth() - 110)) {
        this.lastClick.set(mouse.x, mouse.y);
        this.selectedObject = null;
        Iterator padding = this.level.getObjects("actions").iterator();
        
        while (padding.hasNext()) {
          MapObject p = (MapObject)padding.next();
          if (p.getBounds().contains(mouse)) {
            this.selectedObject = new String(p.getName());
            break;
          }
        }
        
        Array var16 = this.level.getSquads();
        
        for (int var17 = 0; var17 < var16.size; var17++) {
          Squad dy = (Squad)var16.get(var17);
          if (dy.isEnemy()) {
            Vector2 gold = camera.project(dy.getLeader().getBounds().getCenter());
            if (gold.dst(com.waleed.KnightsAndArrows.Input.getMousePosition(true)) < 68.0F) {
              this.selectedObject = dy;
            }
          }
        }
        
        if ((this.selectedObject == null) && (this.level.getRedBase().getBounds().contains(mouse))) {
          this.selectedObject = this.level.getRedBase();
        }
        
        if ((this.selectedObject != null) && (!(this.selectedObject instanceof Squad))) {
          if ((this.selectedObject instanceof String)) {
            String var18 = (String)this.selectedObject;
            if (var18.equals("baracks")) {
              if (this.cMenu) {
                this.cMenu = false;
                this.busy = false;
              } else {
                this.busy = true;
                this.cMenu = true;
                setCMenu(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
                Resources.GUISOUNDS[1].play(1.0F);
              }
            } else if (var18.equals("base")) {
              if (this.bMenu) {
                this.bMenu = false;
                this.busy = false;
              } else {
                this.busy = true;
                this.bMenu = true;
                setBMenu(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
                Resources.GUISOUNDS[1].play(1.0F);
              }
            }
          } else if ((this.selectedObject instanceof Base)) {
            Base var21 = (Base)this.selectedObject;
            if (var21.isEnemy()) {
              if (this.aMenu) {
                this.aMenu = false;
                this.busy = false;
              } else {
                this.busy = true;
                this.aMenu = true;
                this.aStage = 0.0F;
              }
            } else if (this.bMenu) {
              this.bMenu = false;
              this.busy = false;
            } else {
              this.busy = true;
              this.bMenu = true;
              setBMenu(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
              Resources.GUISOUNDS[1].play(1.0F);
            }
          }
        } else if (this.aMenu) {
          this.aMenu = false;
          this.busy = false;
        } else {
          this.busy = true;
          this.aMenu = true;
          this.aStage = 0.0F;
        }
      }
      

      if (com.waleed.KnightsAndArrows.Input.isKeyPressed(129)) {
        Iterator padding = this.level.getSquads().iterator();
        
        while (padding.hasNext()) {
          Squad var15 = (Squad)padding.next();
          if (!var15.isEnemy()) {
            var15.setSelected(!var15.isSelected());
          }
        }
      }
      
      if (com.waleed.KnightsAndArrows.Input.isKeyPressed(29)) {
        Iterator padding = this.level.getSquads().iterator();
        
        while (padding.hasNext()) {
          Squad var15 = (Squad)padding.next();
          if (!var15.isEnemy()) {
            var15.setSelected(!this.selectAll);
          }
        }
        
        this.selectAll = (!this.selectAll);
      }
      
      if (com.waleed.KnightsAndArrows.Input.getMouseX() < Gdx.graphics.getWidth() - 110) {
        if (com.waleed.KnightsAndArrows.Input.isButtonDown(0)) {
          if (com.waleed.KnightsAndArrows.Input.hasMouseMoved()) {
            if (this.sReady) {
              this.sReady = false;
              this.ox = mouse.x;
              this.oy = mouse.y;
              this.sMode = 0;
              this.sMode = (com.waleed.KnightsAndArrows.Input.isKeyDown(59) ? 1 : this.sMode);
            } else {
              this.selection.set(this.ox, this.oy, mouse.x - this.ox, mouse.y - this.oy);
              this.selection.fixNegativeSize();
            }
          }
        } else if (!this.sReady) {
          if ((this.selection.width > 1.0F) && (this.selection.height > 1.0F)) {
            Array var16 = this.level.getSquads();
            Iterator var22 = var16.iterator();
            
            while (var22.hasNext()) {
              Squad var25 = (Squad)var22.next();
              if (!var25.isEnemy()) {
                if (this.sMode == 0) {
                  var25.setSelected(false);
                }
                
                if (this.selection.contains(var25.getLeader().getBounds().getCenter())) {
                  var25.setSelected(true);
                }
              }
            }
          }
          
          this.sReady = true;
          this.ox = 0.0F;
          this.oy = 0.0F;
          this.selection.set(0.0F, 0.0F, 0.0F, 0.0F);
        }
      } else {
        this.sReady = true;
        this.ox = 0.0F;
        this.oy = 0.0F;
        this.selection.set(0.0F, 0.0F, 0.0F, 0.0F);
      }
      
      Gdx.gl.glEnable(3042);
      Gdx.gl.glBlendFunc(770, 771);
      String var19 = "";
      boolean var29 = false;
      Iterator var24 = this.level.getEntities().iterator();
      
      while (var24.hasNext()) {
        Entity var20 = (Entity)var24.next();
        if (var20.getBounds().contains(mouse)) {
          if ((var20 instanceof Unit)) {
            var29 = true;
            Unit tb = (Unit)var20;
            var19 = tb.getClass().getSimpleName();
            float o = tb.getStats().attackRange / 32.0F;
            var19 = var19 + "\nRange: " + (int)(o * 100.0F) / 100.0F;
            var19 = var19 + "\nRate: " + (int)(1.0F / tb.getStats().attackRate * 100.0F) / 100.0F;
            var19 = var19 + "\nHP: " + tb.getStats().hp + "/" + tb.getStats().maxHp;
            BitmapFont.TextBounds tb1 = Resources.FONT.getMultiLineBounds(var19);
            this.sr.setColor(0.0F, 0.0F, 0.0F, 0.65F);
            this.sr.begin(ShapeRenderer.ShapeType.Filled);
            this.sr.rect(mouse.x - 5.0F, mouse.y - 5.0F, tb1.width + 10.0F, tb1.height + 10.0F);
            this.sr.end();
            batch.begin();
            Resources.FONT.drawMultiLine(batch, var19, com.waleed.KnightsAndArrows.Input.getMouseX(), com.waleed.KnightsAndArrows.Input.getMouseY() + tb1.height);
            batch.end();
            break; } if ((var20 instanceof Base)) {
            Base var36 = (Base)var20;
            if (var36.isEnemy()) {
              var19 = "Red Base";
              break; }
            var29 = true;
            var19 = "Blue Base";
            BitmapFont.TextBounds var35 = Resources.FONT.getBounds(var19);
            this.sr.setColor(0.0F, 0.0F, 0.0F, 0.65F);
            this.sr.begin(ShapeRenderer.ShapeType.Filled);
            this.sr.rect(mouse.x - 5.0F, mouse.y - 5.0F, var35.width + 10.0F, var35.height + 10.0F);
            this.sr.end();
            batch.begin();
            batch.draw(Resources.GUI, com.waleed.KnightsAndArrows.Input.getMouseX() + var35.width + 10.0F, com.waleed.KnightsAndArrows.Input.getMouseY() - 10, 0.0F, 0.0F, 32.0F, 32.0F, 1.0F, 1.0F, 0.0F, 32, 112, 16, 16, false, false);
            Resources.FONT.drawMultiLine(batch, var19, com.waleed.KnightsAndArrows.Input.getMouseX(), com.waleed.KnightsAndArrows.Input.getMouseY() + var35.height);
            batch.end();
            
            break; } if ((var20 instanceof com.waleed.KnightsAndArrows.core.entities.Potato)) {
            var19 = "Potato";
            break; } if ((var20 instanceof com.waleed.KnightsAndArrows.core.entities.Torch)) {
            var19 = "Torch";
            break; } if (!(var20 instanceof com.waleed.KnightsAndArrows.core.entities.Campfire)) break;
          var29 = true;
          var19 = "Campfire";
          BitmapFont.TextBounds var37 = Resources.FONT.getBounds(var19);
          this.sr.setColor(0.0F, 0.0F, 0.0F, 0.65F);
          this.sr.begin(ShapeRenderer.ShapeType.Filled);
          this.sr.rect(mouse.x - 5.0F, mouse.y - 5.0F, var37.width + 10.0F, var37.height + 10.0F);
          this.sr.end();
          batch.begin();
          batch.draw(Resources.GUI, com.waleed.KnightsAndArrows.Input.getMouseX() + var37.width + 10.0F, com.waleed.KnightsAndArrows.Input.getMouseY() - 10, 0.0F, 0.0F, 32.0F, 32.0F, 1.0F, 1.0F, 0.0F, 16, 112, 16, 16, false, false);
          Resources.FONT.drawMultiLine(batch, var19, com.waleed.KnightsAndArrows.Input.getMouseX(), com.waleed.KnightsAndArrows.Input.getMouseY() + var37.height);
          batch.end();
          
          break;
        }
      }
      
      if ((!var19.equals("")) && (!var29)) {
        BitmapFont.TextBounds var26 = Resources.FONT.getBounds(var19);
        this.sr.setColor(0.0F, 0.0F, 0.0F, 0.65F);
        this.sr.begin(ShapeRenderer.ShapeType.Filled);
        this.sr.rect(mouse.x - 5.0F, mouse.y - 5.0F, var26.width + 10.0F, var26.height + 10.0F);
        this.sr.end();
        batch.begin();
        Resources.FONT.drawMultiLine(batch, var19, com.waleed.KnightsAndArrows.Input.getMouseX(), com.waleed.KnightsAndArrows.Input.getMouseY() + var26.height);
        batch.end();
      }
    }
    
    Gdx.gl.glEnable(3042);
    Gdx.gl.glBlendFunc(770, 771);
    if (!this.sReady) {
      this.sr.begin(ShapeRenderer.ShapeType.Line);
      this.sr.setColor(1.0F, 1.0F, 1.0F, 0.8F);
      this.sr.rect(this.selection.x, this.selection.y, this.selection.width, this.selection.height);
      this.sr.end();
      this.sr.begin(ShapeRenderer.ShapeType.Filled);
      this.sr.setColor(1.0F, 1.0F, 1.0F, 0.1F);
      this.sr.rect(this.selection.x, this.selection.y, this.selection.width, this.selection.height);
      this.sr.end();
    }
    

    if (this.aMenu) {
      this.aStage = MathUtils.lerp(this.aStage, 1.0F, delta * 6.0F);
      Rectangle[] var33 = new Rectangle[3];
      Vector2 var34 = camera.project(this.lastClick);
      var33[0] = new Rectangle(var34.x + 32.0F, var34.y - 32.0F, 64.0F, 64.0F);
      var33[1] = new Rectangle(var34.x - 32.0F, var34.y + 32.0F, 64.0F, 64.0F);
      var33[2] = new Rectangle(var34.x - 96.0F, var34.y - 32.0F, 64.0F, 64.0F);
      batch.begin();
      boolean var23 = com.waleed.KnightsAndArrows.Input.isButtonPressed(0);
      
      for (int var27 = 0; var27 < 3; var27++) {
        boolean var30 = var33[var27].contains(com.waleed.KnightsAndArrows.Input.getMousePosition(true));
        if (var23) {
          this.busy = false;
          if (var30) {
            this.aMenu = false;
            Array var40 = this.level.getSquads();
            
            int var43;
            
            switch (var27) {
            case 0: 
              var43 = 0;
            case 1: 
            case 2: 
            	int var431 = 0;
              while (var431 < var40.size)
              {


                Squad var49 = (Squad)var40.get(var431);
                if (var49.isSelected()) {
                  var49.flee();
                }
                
                var431++;
                                

                while (var431 < var40.size)
                {


                  Squad var50 = (Squad)var40.get(var431);
                  if (var50.isSelected()) {
                	  var50.setTarget((int)(this.lastClick.x / 32.0F), (int)(this.lastClick.y / 32.0F));
                  }
                  
                  var431++;
                  



                  if ((this.selectedObject instanceof Squad)) {
                    Squad var38 = (Squad)this.selectedObject;
                    
                    for (int name = 0; name < var40.size; name++) {
                      Squad pos = (Squad)var40.get(name);
                      if (pos.isSelected()) {
                        pos.startAttack(var38);
                      }
                    }
                  } else if ((this.selectedObject instanceof Base)) {
                    Base var39 = (Base)this.selectedObject;
                    
                    for (int name = 0; name < var40.size; name++) {
                      Squad pos = (Squad)var40.get(name);
                      if (pos.isSelected())
                        pos.startBaseAttack(var39);
                    }
                  }
                }
              }
            }
            com.waleed.KnightsAndArrows.Input.restUps();
          }
        }
        
        float o = 320.0F + 40.0F * (this.aStage * 4.0F - var27);
        if (o > 360.0F) {
          o = 360.0F;
        }
        
        batch.draw(Resources.ACTIONS, var33[var27].x, var33[var27].y, var33[var27].width / 2.0F, var33[var27].height / 2.0F, var33[var27].width, var33[var27].height, this.aStage, this.aStage, o, var27 << 4, var30 ? 0 : 16, 16, 16, false, false);
        if (var30) {
          String var48 = "";
          switch (var27) {
          case 0: 
            var48 = "Abort";
            break;
          case 1: 
            var48 = "Move";
            break;
          case 2: 
            var48 = "Attack";
          }
          
          BitmapFont.TextBounds var46 = Resources.FONT.getBounds(var48);
          Resources.FONT.draw(batch, var48, var34.x - var46.width / 2.0F, var34.y + Resources.FONT.getLineHeight() / 2.0F);
        }
      }
      
      batch.end();
    }
    
    if ((!this.cMenu) && (!this.bMenu)) {
      Gdx.input.setInputProcessor(com.waleed.KnightsAndArrows.KnightsAndArrows.INPUT);
    } else {
      batch.begin();
      batch.draw(Resources.GUI, 0.0F, 0.0F, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), 127, 127, 1, 1, false, false);
      batch.end();
      Gdx.input.setInputProcessor(this.stage);
      this.stage.act(delta);
      this.stage.draw();
    }
    
    Gdx.gl.glEnable(3042);
    Gdx.gl.glBlendFunc(770, 771);
    this.sr.begin(ShapeRenderer.ShapeType.Filled);
    this.sr.setColor(0.0F, 0.0F, 0.0F, 0.1F);
    Vector2 var41 = camera.unproject(new Vector2(Gdx.graphics.getWidth() - 110, Gdx.graphics.getHeight()));
    this.sr.rect(var41.x, var41.y, 110.0F, Gdx.graphics.getHeight());
    this.sr.end();
    byte var42 = 5;
    int var28 = 0;
    

    for (int var27 = 0; var27 < this.level.getSquads().size; var27++) {
      Squad var32 = (Squad)this.level.getSquads().get(var27);
      if (!var32.isEnemy()) {
        Rectangle var44 = new Rectangle(Gdx.graphics.getWidth() - var42 - 96, Gdx.graphics.getHeight() - (48 + var42) * (var28 + 1) - 48, 96.0F, 48.0F);
        batch.begin();
        if (var44.contains(com.waleed.KnightsAndArrows.Input.getMousePosition(true))) {
          batch.setColor(Color.GRAY);
          if ((com.waleed.KnightsAndArrows.Input.isButtonDown(0)) && (this.sReady)) {
            camera.setTarget(var32.getPosition().x, var32.getPosition().y);
          }
        }
        
        batch.draw(Resources.GUI, var44.x, var44.y, var44.width, var44.height, 0, var32.isInCombat() ? 91 : 79, 24, 12, false, false);
        batch.setColor(Color.WHITE);
        BitmapFont.TextBounds tb1 = Resources.FONT.getBounds(var32.getName());
        Resources.FONT.draw(batch, var32.getName(), var44.x + 48.0F - tb1.width / 2.0F, var44.y + 48.0F - tb1.height);
        String var47 = var32.getMembers().size + "/" + 9;
        tb1 = Resources.FONT.getBounds(var47);
        Resources.FONT.draw(batch, var47, var44.x + 48.0F - tb1.width / 2.0F, var44.y + 32.0F - tb1.height);
        batch.end();
        var28++;
      }
    }
    
    String var31 = this.level.gold + "G";
    BitmapFont.TextBounds var37 = Resources.FONT.getBounds(var31);
    batch.begin();
    Resources.FONT.setColor(Color.YELLOW);
    Resources.FONT.draw(batch, var31, Gdx.graphics.getWidth() - 55 - var37.width / 2.0F, Gdx.graphics.getHeight() - 24);
    Resources.FONT.setColor(Color.WHITE);
    batch.end();
    if ((!this.bMenu) && (!this.cMenu)) {
      Iterator var51 = this.level.getObjects("actions").iterator();
      
      while (var51.hasNext()) {
        MapObject var45 = (MapObject)var51.next();
        String var47 = new String(var45.getName());
        if (var47.equals("baracks")) {
          Vector2 var50 = camera.project(new Vector2(Integer.parseInt(var45.getProperties().get("x").toString()) << 2, Integer.parseInt(var45.getProperties().get("y").toString()) << 2));
          batch.begin();
          batch.draw(Resources.GUI, var50.x + 8.0F, var50.y + 96.0F, 16.0F, 16.0F, 80, 112, 16, 16, false, false);
          batch.end();
        }
      }
    }
  }
  
  public void resize(int width, int height)
  {
    if (this.first) {
      this.style = new com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle();
      this.style.up = Resources.BUTTONS.getDrawable("button_off");
      this.style.over = Resources.BUTTONS.getDrawable("button_on");
      this.style.down = Resources.BUTTONS.getDrawable("button_active");
      this.style.font = Resources.FONT;
    }
    
    if (this.cMenu) {
      setCMenu(width, height);
    }
    
    if (this.bMenu) {
      setBMenu(width, height);
    }
    
    this.first = false;
  }
  
  private void addUnit(int id) {
    boolean added = false;
    if (this.level.gold >= 10) {
      Iterator var4 = this.level.getSquads().iterator();
      
      while (var4.hasNext()) {
        Squad s = (Squad)var4.next();
        if ((s.isSelected()) && (s.getMembers().size < 9)) {
          switch (id) {
          case 0: 
            s.addToFreeSpot(new com.waleed.KnightsAndArrows.core.entities.units.Magician());
            break;
          case 1: 
            s.addToFreeSpot(new com.waleed.KnightsAndArrows.core.entities.units.Knight());
            break;
          case 2: 
            s.addToFreeSpot(new com.waleed.KnightsAndArrows.core.entities.units.Bowman());
            break;
          case 3: 
            s.addToFreeSpot(new com.waleed.KnightsAndArrows.core.entities.units.Monk());
          }
          
          this.level.gold -= 10;
          added = true;
          break;
        }
      }
    }
    
    Resources.GUISOUNDS[2].play();
  }
  
  private void createNewSquad(String name) {
    this.level.gold -= 100;
    Squad squad = new Squad(this.level.getBlueBase().getBounds().x, this.level.getBlueBase().getBounds().y + 64.0F, this.level, name);
    squad.addRandomMembers(1);
    float tx = this.level.squadCreateArea.x + MathUtils.random() * this.level.squadCreateArea.width;
    float ty = this.level.squadCreateArea.y + MathUtils.random() * this.level.squadCreateArea.height;
    squad.setTarget((int)(tx / 32.0F), (int)(ty / 32.0F));
  }
  
  private void hideShop() {
    this.cMenu = false;
    this.busy = false;
  }
  
  private void hideBase() {
    this.bMenu = false;
    this.busy = false;
  }
  
  public boolean isBusy() {
    return this.busy;
  }
  
  private void setBMenu(int width, int height)
  {
    this.stage = new Stage(width, height, true);
    this.stage.clear();
    TextureRegion bg = new TextureRegion(Resources.GUI);
    bg.setRegion(0, 48, 55, 31);
    Image image = new Image(new TextureRegionDrawable(bg));
    image.setSize(440.0F, 248.0F);
    float x = width / 2 - 220;
    float y = height / 2 - 124;
    image.setPosition(x, y);
    this.stage.addActor(image);
    TextButton create = new TextButton("Create (100G)", this.style);
    create.setSize(128.0F, 48.0F);
    create.setPosition(x + 160.0F, y + 12.0F);
    create.addListener(new InputListener()
    {
      public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) { return true; }
      
      public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        if (button == 0) {
          if ((GuiHandler.this.level.gold >= 100) && (GuiHandler.this.level.getSquadCount(false) < 10)) {
            String text = GuiHandler.this.name.getText();
            text = text.substring(1, text.length());
            if ((!text.equals("")) && (!text.equals(" "))) {
              GuiHandler.this.createNewSquad(text);
              Resources.GUISOUNDS[1].play();
              GuiHandler.this.hideBase();
            } else {
              Resources.GUISOUNDS[2].play();
            }
          } else {
            Resources.GUISOUNDS[2].play();
          }
        }
        
        super.touchUp(event, x, y, pointer, button);
      }
    });
    this.stage.addActor(create);
    TextButton close = new TextButton("Close", this.style);
    close.setSize(128.0F, 48.0F);
    close.setPosition(x + 300.0F, y + 12.0F);
    close.addListener(new InputListener()
    {
      public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) { return true; }
      
      public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        if (button == 0) {
          Resources.GUISOUNDS[1].play();
          GuiHandler.this.hideBase();
        }
        
        super.touchUp(event, x, y, pointer, button);
      }
    });
    this.stage.addActor(close);
    Label.LabelStyle lstyle = new Label.LabelStyle();
    lstyle.font = Resources.FONT;
    Label label = new Label("Create a new squad. (max 10)", lstyle);
    label.setPosition(this.stage.getWidth() / 2.0F - 220.0F + 32.0F, this.stage.getHeight() / 2.0F + 48.0F);
    this.stage.addActor(label);
    Label label2 = new Label("Name: ", lstyle);
    label2.setPosition(this.stage.getWidth() / 2.0F - 220.0F + 32.0F, this.stage.getHeight() / 2.0F);
    this.stage.addActor(label2);
    com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldStyle fieldStyle = new com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldStyle();
    fieldStyle.background = Resources.BUTTONS.getDrawable("field_bg");
    fieldStyle.disabledBackground = Resources.BUTTONS.getDrawable("field_bg");
    fieldStyle.focusedBackground = Resources.BUTTONS.getDrawable("field_focus");
    fieldStyle.cursor = Resources.BUTTONS.getDrawable("cursor");
    fieldStyle.font = Resources.FONT;
    fieldStyle.fontColor = Color.WHITE;
    this.name = new TextField(" ", fieldStyle);
    this.name.setCursorPosition(1);
    this.name.setMaxLength(9);
    this.name.setBlinkTime(0.5F);
    this.name.setPosition(this.stage.getWidth() / 2.0F - 135.0F, this.stage.getHeight() / 2.0F - 7.0F);
    this.name.setSize(192.0F, 32.0F);
    this.name.addListener(new InputListener() {
      public boolean keyTyped(InputEvent event, char character) {
        TextField field = (TextField)event.getListenerActor();
        if (field.getText().length() == 0) {
          field.setText(" ");
          field.setCursorPosition(1);
        }
        
        return super.keyTyped(event, character);
      }
      
      public boolean keyUp(InputEvent event, int keycode) { if (keycode == 66) {
          if ((GuiHandler.this.level.gold >= 100) && (GuiHandler.this.level.getSquadCount(false) < 10)) {
            String text = GuiHandler.this.name.getText();
            text = text.substring(1, text.length());
            if ((!text.equals("")) && (!text.equals(" "))) {
              GuiHandler.this.createNewSquad(text);
              Resources.GUISOUNDS[1].play();
              GuiHandler.this.hideBase();
            } else {
              Resources.GUISOUNDS[2].play();
            }
          } else {
            Resources.GUISOUNDS[2].play();
          }
        }
        
        return super.keyUp(event, keycode);
      }
    });
    this.stage.addActor(this.name);
  }
  
  private void setCMenu(int width, int height) {
    this.stage = new Stage(width, height, true);
    this.stage.clear();
    TextureRegion bg = new TextureRegion(Resources.GUI);
    bg.setRegion(0, 48, 55, 31);
    Image image = new Image(new TextureRegionDrawable(bg));
    image.setSize(440.0F, 248.0F);
    float x = width / 2 - 220;
    float y = height / 2 - 124;
    image.setPosition(x, y);
    this.stage.addActor(image);
    Rectangle[] icons = new Rectangle[4];
    
    for (int imgStyle = 0; imgStyle < 4; imgStyle++) {
      icons[imgStyle] = new Rectangle(x + imgStyle * 96 + 32.0F, y + 248.0F - 96.0F, 64.0F, 64.0F);
    }
    
    ImageButton.ImageButtonStyle var14 = new ImageButton.ImageButtonStyle();
    var14.up = Resources.BUTTONS.getDrawable("plus_off");
    var14.down = Resources.BUTTONS.getDrawable("plus_on");
    var14.over = Resources.BUTTONS.getDrawable("plus_on");
    

    for (int okButton = 0; okButton < 4; okButton++) {
      final int z = okButton;
      ImageButton lstyle = new ImageButton(var14);
      lstyle.setSize(32.0F, 32.0F);
      lstyle.setPosition(icons[okButton].x + 16.0F, icons[okButton].y - 48.0F);
      lstyle.addListener(new InputListener()
      {
        public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) { return true; }
        
        public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
          if (button == 0) {
            GuiHandler.this.addUnit(z);
          }
          
          super.touchUp(event, x, y, pointer, button);
        }
      });
      this.stage.addActor(lstyle);
    }
    
    for (int ok = 0; ok < 2; ok++) {
      for (int var17 = 0; var17 < 2; var17++) {
        TextureRegion addlabel = new TextureRegion(Resources.GUI);
        addlabel.setRegion(64 + ok * 8, 112 + var17 * 8, 8, 8);
        Image i = new Image(new TextureRegionDrawable(addlabel));
        i.setSize(64.0F, 64.0F);
        Rectangle cost = icons[(ok + var17 * 2)];
        i.setPosition(cost.x, cost.y);
        this.stage.addActor(i);
      }
    }
    
    TextButton var18 = new TextButton("Ok", this.style);
    var18.setSize(128.0F, 48.0F);
    var18.setPosition(x + 300.0F, y + 12.0F);
    var18.addListener(new InputListener()
    {
      public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) { return true; }
      
      public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        if (button == 0) {
          Resources.GUISOUNDS[1].play();
          GuiHandler.this.hideShop();
        }
        
        super.touchUp(event, x, y, pointer, button);
      }
    });
    this.stage.addActor(var18);
    Label.LabelStyle var15 = new Label.LabelStyle();
    var15.font = Resources.FONT;
    Label var16 = new Label("Add units to selected squad.", var15);
    var16.setPosition(this.stage.getWidth() / 2.0F - 220.0F + 32.0F, this.stage.getHeight() / 2.0F - 96.0F);
    this.stage.addActor(var16);
    
    for (int var19 = 0; var19 < 4; var19++) {
      Label var20 = new Label("10G", var15);
      var20.setPosition(this.stage.getWidth() / 2.0F - 220.0F + var19 * 96 + 51.0F, this.stage.getHeight() / 2.0F - 40.0F);
      this.stage.addActor(var20);
    }
  }
  


  public void dispose()
  {
    if (this.stage != null) {
      this.stage.dispose();
    }
  }
}
