package com.waleed.KnightsAndArrows.core;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapLayers;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.waleed.KnightsAndArrows.core.entities.Unit;
import com.waleed.KnightsAndArrows.pathfinding.AStarPathFinder;
import com.waleed.KnightsAndArrows.pathfinding.PathFindingContext;
import com.waleed.KnightsAndArrows.util.Camera;

public abstract class TileMap implements com.waleed.KnightsAndArrows.pathfinding.TileBasedMap
{
  protected TiledMap map;
  private int tileSize = 32;
  protected OrthogonalTiledMapRenderer mapRenderer;
  
  public TileMap(String path)
  {
    this.map = new TmxMapLoader().load(path);
    this.mapRenderer = new OrthogonalTiledMapRenderer(this.map, 4.0F);
  }
  
  public int getWidthInTiles() {
    return ((Integer)this.map.getProperties().get("width")).intValue();
  }
  
  public int getHeightInTiles() {
    return ((Integer)this.map.getProperties().get("height")).intValue();
  }
  
  public com.waleed.KnightsAndArrows.pathfinding.Path getPath(int sx, int sy, int ex, int ey, int maxDistance, Unit unit) {
    AStarPathFinder finder = new AStarPathFinder(this, maxDistance, true);
    return (sx >= 0) && (sy >= 0) && (ex >= 0) && (ey >= 0) ? finder.findPath(unit, sx, sy, ex, ey) : null;
  }
  
  public TiledMap getMap() {
    return this.map;
  }
  
  public void pathFinderVisited(int x, int y) {}
  
  public boolean isBlocked(int tx, int ty) {
    TiledMapTileLayer walls = (TiledMapTileLayer)this.map.getLayers().get("collision");
    return walls.getCell(tx, ty) != null;
  }
  
  public boolean blocked(PathFindingContext context, int tx, int ty) {
    int sx = context.getSourceX();
    int sy = context.getSourceY();
    return (tx != sx) && (ty != sy) ? false : (sx > tx) && (sy > ty) ? true : (!isBlocked(sx - 1, sy)) && (!isBlocked(sx, sy - 1)) ? false : (sx < tx) && (sy > ty) ? true : (!isBlocked(sx + 1, sy)) && (!isBlocked(sx, sy - 1)) ? false : (sx < tx) && (sy < ty) ? true : (!isBlocked(sx + 1, sy)) && (!isBlocked(sx, sy + 1)) ? false : (sx > tx) && (sy < ty) ? true : (!isBlocked(sx - 1, sy)) && (!isBlocked(sx, sy + 1)) ? false : isBlocked(tx, ty) ? true : isBlocked(tx, ty);
  }
  
  public float getCost(PathFindingContext context, int tx, int ty) {
    float dx = context.getSourceX() - tx;
    float dy = context.getSourceY() - ty;
    return dx * dx + dy * dy;
  }
  
  public boolean canSee(Vector2 v1, Vector2 v2, float maxDistance) {
    if (maxDistance != -1.0F) {
      float ray = v1.dst(v2);
      if (ray > maxDistance) {
        return false;
      }
    }
    
    Vector2 ray1 = castRay(v1, v2);
    return ray1.equals(v2);
  }
  
  public Vector2 castRay(Vector2 v1, Vector2 v2) {
    Vector2 p1 = new Vector2(v1.x / this.tileSize, v1.y / this.tileSize);
    Vector2 p2 = new Vector2(v2.x / this.tileSize, v2.y / this.tileSize);
    if (((int)p1.x == (int)p2.x) && ((int)p1.y == (int)p2.y))
      return v2;
    if ((p1.x >= 0.0F) && (p1.x <= getWidthInTiles()) && (p1.y >= 0.0F) && (p1.y <= getHeightInTiles()) && (p2.x >= 0.0F) && (p2.x <= getWidthInTiles()) && (p2.y >= 0.0F) && (p2.y <= getHeightInTiles())) {
      int stepX = p2.x > p1.x ? 1 : -1;
      int stepY = p2.y > p1.y ? 1 : -1;
      Vector2 rayDirection = new Vector2(p2.x - p1.x, p2.y - p1.y);
      float ratioX = rayDirection.x / rayDirection.y;
      float ratioY = rayDirection.y / rayDirection.x;
      float deltaY = p2.x - p1.x;
      float deltaX = p2.y - p1.y;
      deltaX = deltaX < 0.0F ? -deltaX : deltaX;
      deltaY = deltaY < 0.0F ? -deltaY : deltaY;
      int testX = (int)p1.x;
      int testY = (int)p1.y;
      float maxX = deltaX * (stepX > 0 ? 1.0F - p1.x % 1.0F : p1.x % 1.0F);
      float maxY = deltaY * (stepY > 0 ? 1.0F - p1.y % 1.0F : p1.y % 1.0F);
      int endTileX = (int)p2.x;
      int endTileY = (int)p2.y;
      Vector2 collisionPoint = new Vector2();
      
      while ((testX != endTileX) || (testY != endTileY)) {
        if (maxX < maxY) {
          maxX += deltaX;
          testX += stepX;
          if (isBlocked(testX, testY)) {
            collisionPoint.x = testX;
            if (stepX < 0) {
              collisionPoint.x = ((float)(collisionPoint.x + 1.0D));
            }
            
            p1.y += ratioY * (collisionPoint.x - p1.x);
            collisionPoint.x *= this.tileSize;
            collisionPoint.y *= this.tileSize;
            return collisionPoint;
          }
        } else {
          maxY += deltaY;
          testY += stepY;
          if (isBlocked(testX, testY)) {
            collisionPoint.y = testY;
            if (stepY < 0) {
              collisionPoint.y = ((float)(collisionPoint.y + 1.0D));
            }
            
            p1.x += ratioX * (collisionPoint.y - p1.y);
            collisionPoint.x *= this.tileSize;
            collisionPoint.y *= this.tileSize;
            return collisionPoint;
          }
        }
      }
      
      return v2;
    }
    return v1;
  }
  
  public void setView(Camera camera)
  {
    this.mapRenderer.setView(camera);
  }
  
  public void renderLayers(String tag, Matrix4 projection) {
    renderLayers(tag, projection, Color.WHITE);
  }
  
  public void renderLayers(String tag, Matrix4 projection, Color color) {
    this.mapRenderer.getSpriteBatch().begin();
    this.mapRenderer.getSpriteBatch().setProjectionMatrix(projection);
    this.mapRenderer.getSpriteBatch().setColor(color);
    
    for (int i = 0; i < this.map.getLayers().getCount(); i++) {
      MapLayer layer = this.map.getLayers().get(i);
      if (layer.getProperties().containsKey(tag)) {
        this.mapRenderer.renderTileLayer((TiledMapTileLayer)layer);
      }
    }
    
    this.mapRenderer.getSpriteBatch().setColor(Color.WHITE);
    this.mapRenderer.getSpriteBatch().end();
  }
  
  public com.badlogic.gdx.maps.MapObjects getObjects(String layer) {
    return this.map.getLayers().get(layer).getObjects();
  }
  
  public void dispose() {
    this.map.dispose();
    this.mapRenderer.dispose();
  }
}
