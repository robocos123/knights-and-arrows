package com.waleed.KnightsAndArrows.core.entities;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.waleed.KnightsAndArrows.Resources;

public class Potato extends Entity
{
  public Potato(float x, float y)
  {
    this.bounds.set(x, y, 32.0F, 32.0F);
  }
  
  public void update(float delta) {}
  
  public void render(SpriteBatch batch) {
    batch.setColor(getLuminance());
    batch.draw(Resources.getSprite(0, 104), this.bounds.x, this.bounds.y, 32.0F, 32.0F);
    batch.setColor(com.badlogic.gdx.graphics.Color.WHITE);
  }
  

  public int compareTo(Object o)
  {
    return 0;
  }
}
