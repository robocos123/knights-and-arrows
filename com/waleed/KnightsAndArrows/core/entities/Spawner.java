package com.waleed.KnightsAndArrows.core.entities;

import com.badlogic.gdx.math.Rectangle;
import com.waleed.KnightsAndArrows.core.Level;


public abstract class Spawner
{
  private static byte next;
  protected byte id;
  protected int delay;
  protected int max;
  protected Entity target;
  protected float last;
  protected Rectangle bounds;
  
  public Spawner(int x, int y, int w, int h, int delay, int max, Entity target)
  {
    this.id = (next++);
    this.last = delay;
    this.delay = delay;
    this.max = max;
    this.target = target;
    this.bounds = new Rectangle(x, y, w, h);
  }
  
  public abstract void update(float paramFloat, Level paramLevel);
}
