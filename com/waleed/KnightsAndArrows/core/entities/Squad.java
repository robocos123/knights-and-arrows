package com.waleed.KnightsAndArrows.core.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GLCommon;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.waleed.KnightsAndArrows.Input;
import com.waleed.KnightsAndArrows.Resources;
import com.waleed.KnightsAndArrows.core.Base;
import com.waleed.KnightsAndArrows.core.Level;
import com.waleed.KnightsAndArrows.core.entities.units.Archer;
import com.waleed.KnightsAndArrows.core.entities.units.Bowman;
import com.waleed.KnightsAndArrows.core.entities.units.Knight;
import com.waleed.KnightsAndArrows.core.entities.units.Lich;
import com.waleed.KnightsAndArrows.core.entities.units.Magician;
import com.waleed.KnightsAndArrows.core.entities.units.Monk;
import com.waleed.KnightsAndArrows.core.entities.units.Necro;
import com.waleed.KnightsAndArrows.core.entities.units.Warrior;
import com.waleed.KnightsAndArrows.pathfinding.Path;
import com.waleed.KnightsAndArrows.util.Animation;
import com.waleed.KnightsAndArrows.util.Camera;
import java.io.PrintStream;
import java.util.Iterator;
import java.util.List;
import org.lwjgl.opengl.GL11;

public class Squad
{
	public static final byte MAX_SIZE = 9;
	public static final byte MAX_COUNT = 10;
	private static int next;
	private int id;
	private String name;
	private Animation flag;
	private boolean enemy;
	private EnemyType type;
	public float lastAttack;
	public byte spawnerID;
	private boolean selected;
	private Level level;
	private Vector2 position;
	private Path path;
	private boolean inCombat;
	private Array<Object> members;
	public Array<Object> targets;
	private Base baseTarget;
	public static boolean hoverEnemy;

	public Squad(float x, float y, Level level, String name)
	{
		this(x, y, level, false, name);
	}

	public Squad(float x, float y, Level level, boolean enemy) {
		this(x, y, level, enemy, (String)Resources.NAMES.get((int)(Math.random() * Resources.NAMES.size())));
	}

	public Squad(float x, float y, Level level) {
		this(x, y, level, false, (String)Resources.NAMES.get((int)(Math.random() * Resources.NAMES.size())));
	}

	public Squad(float x, float y, Level level, EnemyType type) {
		this(x, y, level, false, (String)Resources.NAMES.get((int)(Math.random() * Resources.NAMES.size())));
		this.type = type;
	}

	public Squad(float x, float y, Level level, boolean enemy, String name)
	{
		this.members = new Array();
		this.targets = new Array();
		this.id = (++next);
		this.enemy = enemy;
		this.name = name;
		this.level = level;
		level.getSquads().add(this);
		this.position = new Vector2(x, y);
		this.flag = new Animation(2, 0.3F);
	}

	public int getID()
	{
		return this.id;
	}

	public String getName() {
		return this.name;
	}

	public EnemyType getEnemyType() {
		return this.type;
	}

	public void setEnemyType(EnemyType type) {
		this.type = type;
	}

	public void add(Unit u) {
		if (this.members.size < 9) {
			this.level.add(u);
			u.enter(this.level);
			u.join(this);
			if (this.members.size <= 0)
			{
				u.setLeader(this.members.size <= 0);
			}
			u.setPosition(this.position.x, this.position.y);
			this.members.add(u);
		}
	}

	public void addToFreeSpot(Unit u)
	{
		if (this.members.size < 9) {
			this.level.add(u);
			u.enter(this.level);
			u.join(this);
			this.members.add(u);
			Vector2 c = new Vector2(this.position.x / 32.0F, this.position.y / 32.0F);

			for (int y = -1; y <= 1; y++) {
				for (int x = -1; x <= 1; x++) {
					int tx = (int)c.x + x;
					int ty = (int)c.y + y;
					if (((x != 0) || (y != 0)) && (!this.level.isSpotTaken(tx, ty))) {
						u.setPosition(tx << 5, ty << 5);
						u.setGoal(tx, ty);
					}
				}
			}
		}
	}


	public void update(float delta)
	{
		this.flag.update(delta);
		this.lastAttack -= delta;
		if (this.type == EnemyType.DEFEND) {
			if (!this.inCombat) {
				Iterator<Squad> u = this.level.getSquads().iterator();

				while (u.hasNext()) {
					Squad i = (Squad)u.next();
					if ((!i.isEnemy()) && (this.level.canSee(getLeader().getBounds().getCenter(), i.getLeader().getBounds().getCenter(), 512.0F))) {
						startAttack(i);
						break;
					}
				}
			} else if (this.lastAttack <= 0.0F) {
				flee();
			}
		}


		for (int var9 = this.members.size - 1; var9 >= 0; var9--) {
			Unit var10 = (Unit)this.members.get(var9);

			if (var10.wasRemoved()) {
				this.level.getEntities().removeValue(var10, false);
				Vector2 c = ((Unit)this.members.get(0)).getBounds().getCenter();
				this.members.removeIndex(var9);
				if (this.members.size == 0) {
					Resources.play(6, this.position.x, this.position.y, this.level);
					this.level.add("squaddefeat", c.x, c.y);
					this.level.remove(this);
					Iterator<Squad> x = this.level.getSquads().iterator();

					while (x.hasNext()) {
						Squad y = (Squad)x.next();
						y.stopAttack(this);
					}

					if ((!this.enemy) && (this.level.getSquadCount(false) == 0)) {
						this.level.end(false);
					}

					return;
				}

				if (var10.isLeader()) {
					((Unit)this.members.get(0)).setLeader(true);
				}
			} else if (var10.adjust) {
				var10.adjust = false;
				Vector2 c = new Vector2(this.position.x / 32.0F, this.position.y / 32.0F);

				for (int var11 = -1; var11 <= 1; var11++) {
					for (int var12 = -1; var12 <= 1; var12++) {
						int tx = (int)c.x + var12;
						int ty = (int)c.y + var11;
						if (((var12 != 0) || (var11 != 0)) && (!this.level.isSpotTaken(tx, ty))) {
							var10.setGoal(tx, ty);
						}
					}
				}
			}
		}
	}

	public void renderView(SpriteBatch batch, Camera camera)
	{
		Vector2 sp = camera.project(new Vector2(this.position.x + 16.0F, this.position.y + 16.0F));
		Vector2 leaderPos = camera.project(((Unit)this.members.get(0)).getBounds().getCenter());
		GL11.glLineWidth(2.0F / camera.zoom);
		Gdx.gl.glEnable(3042);
		Gdx.gl.glBlendFunc(770, 771);







		if ((this.inCombat) && (!this.enemy)) {
			Iterator<Object> tb = this.targets.iterator();

			while (tb.hasNext()) {
				Squad text = (Squad)tb.next();
				Vector2 dy = camera.project(text.getLeader().getBounds().getCenter());
				Vector2 w = dy.cpy().sub(sp);
				float dw = w.len() - 140.0F;
				w.nor();
				Gdx.gl.glEnable(3042);
				Gdx.gl.glBlendFunc(770, 771);
				this.level.sr.begin(ShapeRenderer.ShapeType.Line);
				this.level.sr.setColor(1.0F, 0.0F, 0.0F, 0.5F);

				for (float dy1 = 0.0F; dy1 < dw - 16.0F; dy1 += 16.0F) {
					float d2 = 70.0F + dy1;
					float d21 = 70.0F + dy1 + 8.0F;
					this.level.sr.line(sp.x + w.x * d2, sp.y + w.y * d2, sp.x + w.x * d21, sp.y + w.y * d21);
				}

				this.level.sr.end();
				Rectangle dy5 = new Rectangle((dy.x + sp.x) / 2.0F - 16.0F, (dy.y + sp.y) / 2.0F - 16.0F, 32.0F, 32.0F);
				boolean d22 = dy5.contains(Input.getMousePosition(true));
				batch.begin();
				batch.enableBlending();
				batch.setColor(1.0F, 1.0F, 1.0F, d22 ? 1.0F : 0.3F);
				batch.draw(Resources.GUI, dy5.x, dy5.y, dy5.width, dy5.height, 0, 112, 16, 16, false, false);
				batch.end();
				batch.setColor(Color.WHITE);
				if ((d22) && (Input.isButtonDown(2))) {
					stopAttack(text);
				}
			}
		}

		if (!this.enemy) {
			Squad text = null;
			float tb1 = 512.0F;

			Iterator<Squad> w1 = this.level.getSquads().iterator();

			while (w1.hasNext()) {
				Squad dy2 = (Squad)w1.next();
				if ((dy2.isEnemy()) && (!this.targets.contains(dy2, false))) {
					Vector2 dw1 = camera.project(dy2.getLeader().getBounds().getCenter());
					Vector2 dy4 = dw1.cpy().sub(sp);
					float d2 = dy4.len() - 140.0F;
					if (d2 < tb1) {
						tb1 = d2;
						text = dy2;
					}
				}
			}

			if (text != null) {
				Vector2 dy = camera.project(new Vector2(text.getLeader().getBounds().getCenter())).sub(sp);
				Vector2 w = dy.cpy().nor();
				Gdx.gl.glEnable(3042);
				Gdx.gl.glBlendFunc(770, 771);
				this.level.sr.begin(ShapeRenderer.ShapeType.Line);
				this.level.sr.setColor(0.5F, 0.5F, 0.5F, 0.5F);

				for (float dw = 0.0F; dw < tb1 - 16.0F; dw += 16.0F) {
					float dy1 = 70.0F + dw;
					float d2 = 70.0F + dw + 8.0F;
					this.level.sr.line(sp.x + w.x * dy1, sp.y + w.y * dy1, sp.x + w.x * d2, sp.y + w.y * d2);
				}

				this.level.sr.end();
			}
		}

		Gdx.gl.glEnable(3042);
		Gdx.gl.glBlendFunc(770, 771);
		if ((!this.enemy) || (leaderPos.dst(Input.getMousePosition(true)) < 70.0F) || (this.inCombat)) {
			this.level.sr.begin(ShapeRenderer.ShapeType.Filled);
			Iterator<Object> tb = this.members.iterator();

			while (tb.hasNext()) {
				Unit text1 = (Unit)tb.next();
				Vector2 dy = camera.project(text1.getBounds().getCenter());
				this.level.sr.setColor(Color.GREEN);
				float w2 = 16.0F;
				float dw = w2 * text1.getStats().hp / text1.getStats().maxHp;
				float dy1 = -20.0F;
				this.level.sr.rect(dy.x - w2 / 2.0F, dy.y + dy1, dw, 1.0F);
				this.level.sr.setColor(Color.RED);
				this.level.sr.rect(dy.x - w2 / 2.0F + dw, dy.y + dy1, w2 - dw, 1.0F);
			}

			this.level.sr.end();
		}

		String text2 = this.name + " (" + this.members.size + "/" + 9 + ")";
		BitmapFont.TextBounds tb2 = Resources.FONT.getBounds(text2);
		this.level.sr.begin(ShapeRenderer.ShapeType.Filled);
		this.level.sr.setColor(0.0F, 0.0F, 0.0F, 0.3F);
		this.level.sr.rect(leaderPos.x - tb2.width / 2.0F - 4.0F, leaderPos.y + 70.0F, tb2.width + 8.0F, tb2.height + 8.0F);
		this.level.sr.end();
		batch.begin();
		if (this.enemy) {
			batch.draw(Resources.SPRITES[(12 + this.flag.getCurrentFrame())][4], leaderPos.x - 16.0F, leaderPos.y + 90.0F, 16.0F, 16.0F);
		} else {
			float dy3 = 0.0F;
			if (this.position.dst(((Unit)this.members.get(0)).getBounds().getCenter()) < 32.0F) {
				dy3 = 90.0F;
			} else {
				batch.setColor(Color.GRAY);
			}

			batch.draw(Resources.SPRITES[(12 + this.flag.getCurrentFrame())][5], sp.x - 16.0F, sp.y + dy3, 16.0F, 16.0F);
			batch.setColor(Color.WHITE);
		}

		Resources.FONT.draw(batch, text2, leaderPos.x - tb2.width / 2.0F, leaderPos.y + tb2.height + 74.0F);
		batch.end();
		Gdx.gl.glEnable(3042);
		Gdx.gl.glBlendFunc(770, 771);
		if (this.enemy) {
			if ((leaderPos.dst(Input.getMousePosition(true)) < 70.0F) || (this.inCombat)) {
				this.level.sr.setColor(1.0F, 0.0F, 0.0F, 0.5F);
				this.level.sr.setProjectionMatrix(batch.getProjectionMatrix());
				this.level.sr.begin(ShapeRenderer.ShapeType.Line);
				this.level.sr.circle(leaderPos.x, leaderPos.y, 70.0F);
				hoverEnemy = true;
				this.level.sr.end();
			}
		} else if (this.selected) {
			this.level.sr.setColor(1.0F, 1.0F, 1.0F, 0.5F);
			this.level.sr.setProjectionMatrix(batch.getProjectionMatrix());
			this.level.sr.begin(ShapeRenderer.ShapeType.Line);
			this.level.sr.circle(sp.x, sp.y, 70.0F);
			this.level.sr.end();
		}

		Gdx.gl.glDisable(3042);
	}

	public static boolean isOverEnemy()
	{
		return hoverEnemy;
	}

	public void setTarget(int tx, int ty) {
		if (!this.level.isBlocked(tx, ty)) {
			boolean taken = spotTaken(tx, ty);
			int j = 0;

			while (taken) {
				int s = (3 + j * 2) * 2;
				s += s - 4;
				float i = 0.0F;


				while (i < 6.2831855F) {
					float dx = MathUtils.sin(i) * (j + 1);
					float dy = MathUtils.cos(i) * (j + 1);
					if (spotTaken((int)(tx + dx), (int)(ty + dy))) {
						i += 6.2831855F / s;
					}
					else
					{
						taken = false;
						tx = (int)(tx + dx);
						ty = (int)(ty + dy);
					}
				}
				j++;
			}



			Vector2 var9 = ((Unit)this.members.get(0)).getBounds().getCenter();
			this.path = this.level.getPath((int)(var9.x / 32.0F), (int)(var9.y / 32.0F), tx, ty, 256, null);
			if (this.path != null) {
				this.position.set(tx << 5, ty << 5);

				for (int var10 = 0; var10 < this.members.size; var10++) {
					((Unit)this.members.get(var10)).setPath(this.path, var10 * 0.5F);
				}
			}
		}
	}

	public Path getPath()
	{
		return this.path;
	}

	private boolean spotTaken(int x, int y)
	{
		x <<= 5;
		y <<= 5;
		Iterator<Squad> var4 = this.level.getSquads().iterator();

		while (var4.hasNext()) {
			Squad s = (Squad)var4.next();
			if ((this.enemy == s.isEnemy()) && (this.id != s.id)) {
				float dx = s.position.x - x;
				float dy = s.position.y - y;
				if (dx * dx + dy * dy < 15000.0F) {
					return true;
				}
			}
		}

		return false;
	}

	public Vector2 getPosition() {
		return this.position;
	}

	public Array<?> getMembers() {
		return this.members;
	}

	public boolean hasMembers() {
		return this.members.size > 0;
	}

	public Unit getLeader() {
		if (hasMembers()) {
			return (Unit)this.members.get(0);
		}
		System.out.println("Problem solved");
		if (this.members.size <= 8)
		{
			addRandomMembers(1);
		}
		return (Unit)this.members.get(0);
	}

	public void setSelected(boolean b) {
		this.selected = b;
	}

	public boolean isSelected() {
		return this.selected;
	}

	public boolean equals(Object obj) {
		return obj != null;
	}

	public boolean isEnemy() {
		return this.enemy;
	}

	public boolean isInCombat() {
		return this.inCombat;
	}

	public void startAttack(Squad s) {
		if (!this.targets.contains(s, false)) {
			this.targets.add(s);
			this.inCombat = true;
		}
	}


	public void startBaseAttack(Base base)
	{
		this.baseTarget = base;
	}

	public void stopAttack(Squad s) {
		this.targets.removeValue(s, false);
		this.inCombat = (this.targets.size > 0);
		hoverEnemy = this.targets.size > 0;
	}

	public void stopBaseAttack() {
		this.baseTarget = null;
	}

	public Base getBaseTarget() {
		return this.baseTarget;
	}

	public Array<Object> getTargets() {
		return this.targets;
	}

	public void addRandomMembers(int n) {
		if (n > 9) {
			n = 9;
		}

		for (int i = 0; i < n; i++) {
			int random = MathUtils.random(3);
			switch (random) {
			case 0: 
				add(this.enemy ? new Archer() : new Bowman());
				break;
			case 1: 
				add(this.enemy ? new Warrior() : new Knight());
				break;
			case 2: 
				add(this.enemy ? new Necro() : new Magician());
				break;
			case 3: 
				add(this.enemy ? new Lich() : new Monk());
			}

		}
	}

	public void flee()
	{
		this.baseTarget = null;
		this.targets.clear();
		this.inCombat = false;

		Iterator<Squad> var2 = this.level.getSquads().iterator();

		while (var2.hasNext()) {
			Squad s = (Squad)var2.next();
			if (s.isEnemy() != this.enemy) {
				s.targets.removeValue(this, false);
				s.inCombat = (s.targets.size > 0);
			}
		}
	}
	
	public enum EnemyType {
		ZERG, DEFEND;
	}
}
