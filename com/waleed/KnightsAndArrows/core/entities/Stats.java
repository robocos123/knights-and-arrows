package com.waleed.KnightsAndArrows.core.entities;

public class Stats
{
  public float maxHp;
  public float hp;
  public float attackRate;
  public float attackRange;
}
