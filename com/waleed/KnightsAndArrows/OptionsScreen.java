package com.waleed.KnightsAndArrows;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.Graphics.DisplayMode;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

public class OptionsScreen implements com.badlogic.gdx.Screen
{
  private Stage stage1;
  private Stage stage2;
  private Stage stage3;
  private Skin skin;
  private com.badlogic.gdx.graphics.g2d.TextureAtlas atlas;
  private SpriteBatch batch;
  private com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable mapSelectTexture;
  public byte state = 0;
  
  String chosenString;
  float prevVol;
  public boolean error = false;
  
  public boolean isCancelled = false;
  
  boolean fullScreen;
  
  public static String errorMsg;
  
  boolean wasHigh = true;
  boolean wasMed = false; boolean wasLow = false;
  String shaderState = shaders ? "Shaders: ON" : "Shaders: OFF";
  String soundState = sounds ? "Sounds: ON" : "Sounds: OFF";
  String lightingState = lighting ? "Lighting: ON" : "Lighting: OFF";
  
  public static boolean shaders = true;
  public static boolean lighting = true;
  
  public static boolean music = true; public static boolean sounds = true;
  

  public OptionsScreen()
  {
    this.fullScreen = (Gdx.graphics.isFullscreen());
  }
  
  public void render(float delta) {
    Gdx.gl.glClear(16384);
    Gdx.gl.glClearColor(0.0F, 0.0F, 0.0F, 0.0F);
    Input.update();
    switch (this.state) {
    case 0: 
      Gdx.input.setInputProcessor(this.stage1);
      this.stage1.act(delta);
      this.stage1.draw();
      this.batch.begin();
      short w = 1024;
      short h = 384;
      Resources.FONT.setScale(3.0F);
      
      Resources.FONT.draw(this.batch, "Options", Gdx.graphics.getWidth() / 2 - 128.0F, Gdx.graphics.getHeight() / 2 + 250.0F);
      Resources.FONT.setScale(2.0F);
      this.batch.end();
      break;
    case 1: 
      Gdx.input.setInputProcessor(this.stage2);
      this.stage2.act(delta);
      this.stage2.draw();
      this.batch.begin();
      


      Resources.FONT.setScale(3.0F);
      
      Resources.FONT.draw(this.batch, "Display Options", Gdx.graphics.getWidth() / 2 - 150.0F, Gdx.graphics.getHeight() / 2 + 250.0F);
      Resources.FONT.setScale(2.0F);
      this.batch.end();
      
      break;
    
    case 2: 
      Gdx.input.setInputProcessor(this.stage3);
      this.stage3.act(delta);
      this.stage3.draw();
      this.batch.begin();
      
      Resources.FONT.setScale(3.0F);
      
      Resources.FONT.draw(this.batch, "Audio Options", Gdx.graphics.getWidth() / 2 - 150.0F, Gdx.graphics.getHeight() / 2 + 250.0F);
      Resources.FONT.setScale(2.0F);
      this.batch.end();
    

    case 3: 
      break;
    


    default: 
      this.state = 0;
    }
    
  }
  














  public static void setError(String error)
  {
    errorMsg = error;
  }
  
  public static String getError()
  {
    return errorMsg;
  }
  
  public boolean isError()
  {
    return this.error;
  }
  
  public void resize(int width, int height) {
    this.stage1 = new Stage(width, height, true, this.batch);
    this.stage2 = new Stage(width, height, true, this.batch);
    this.stage3 = new Stage(width, height, true, this.batch);
    
    com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle style = new com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle();
    style.up = this.skin.getDrawable("button_off");
    style.over = this.skin.getDrawable("button_on");
    style.down = this.skin.getDrawable("button_active");
    style.font = Resources.FONT;
    style.font.setScale(2.0F);
    TextButton startButton = new TextButton("Display", style);
    startButton.setSize(256.0F, 96.0F);
    startButton.setPosition(Gdx.graphics.getWidth() / 2 - 128, Gdx.graphics.getHeight() / 2);
    startButton.addListener(new InputListener()
    {
      public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) { return true; }
      
      public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        if (button == 0) {
          Resources.GUISOUNDS[0].play(KnightsAndArrows.VOLUME);
          OptionsScreen.this.state = 1;
        }
        
        super.touchUp(event, x, y, pointer, button);
      }
    });
    this.stage1.addActor(startButton);
    TextButton exitButton = new TextButton("Audio", style);
    exitButton.setSize(256.0F, 96.0F);
    exitButton.setPosition(Gdx.graphics.getWidth() / 2 - 128, Gdx.graphics.getHeight() / 2 - 100);
    exitButton.addListener(new InputListener()
    {
      public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) { return true; }
      
      public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        if (button == 0) {
          Resources.GUISOUNDS[0].play(KnightsAndArrows.VOLUME);
          OptionsScreen.this.state = 2;
        }
        
        super.touchUp(event, x, y, pointer, button);
      }
    });
    this.stage1.addActor(exitButton);
    
    TextButton cancelButton = new TextButton("Back", style);
    cancelButton.setSize(256.0F, 96.0F);
    cancelButton.setPosition(Gdx.graphics.getWidth() / 2 - 128, Gdx.graphics.getHeight() / 2 - 300);
    cancelButton.addListener(new InputListener()
    {
      public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) { return true; }
      
      public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        if (button == 0) {
          Resources.GUISOUNDS[0].play(KnightsAndArrows.VOLUME);
          if (KnightsAndArrows.isOffline()) {
            KnightsAndArrows.INSTANCE.setScreen(new MenuScreen("Guest", false));
          } else {
            KnightsAndArrows.INSTANCE.setScreen(new MenuScreen(SplashScreen.username, true));
          }
        }
        super.touchUp(event, x, y, pointer, button);
      }
      
    });
    this.stage1.addActor(cancelButton);
    String text = !this.fullScreen ? "Fullscreen" : "Windowed";
    TextButton windowOption = new TextButton(text, style);
    windowOption.setSize(256.0F, 96.0F);
    windowOption.setPosition(Gdx.graphics.getWidth() / 2 - 128, Gdx.graphics.getHeight() / 2);
    windowOption.addListener(new InputListener()
    {
      public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) { return true; }
      
      public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        if (button == 0) {
          Resources.GUISOUNDS[0].play(KnightsAndArrows.VOLUME);
          OptionsScreen.this.fullScreen = (!OptionsScreen.this.fullScreen);
          Graphics.DisplayMode currentMode = Gdx.graphics.getDesktopDisplayMode();
          Gdx.graphics.setDisplayMode(currentMode.width, currentMode.height, OptionsScreen.this.fullScreen);
        }
        

        super.touchUp(event, x, y, pointer, button);
      }
    });
    this.stage2.addActor(windowOption);
    


















    final TextButton displayOption = new TextButton(this.shaderState, style);
    displayOption.setSize(256.0F, 96.0F);
    displayOption.setPosition(Gdx.graphics.getWidth() / 2 - 128, Gdx.graphics.getHeight() / 2 - 120);
    displayOption.addListener(new InputListener()
    {
      public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) { return true; }
      
      public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        if (button == 0) {
          Resources.GUISOUNDS[0].play(KnightsAndArrows.VOLUME);
          OptionsScreen.shaders = !OptionsScreen.shaders;
          String shad = OptionsScreen.shaders ? "Shaders: ON" : "Shaders: OFF";
          displayOption.setText(shad);
        }
        

        super.touchUp(event, x, y, pointer, button);
      }
      
    });
    this.stage2.addActor(displayOption);
    



    TextButton backButton = new TextButton("Back", style);
    backButton.setSize(256.0F, 96.0F);
    backButton.setPosition(Gdx.graphics.getWidth() / 2 - 128, Gdx.graphics.getHeight() / 2 - 272);
    backButton.addListener(new InputListener()
    {
      public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) { return true; }
      
      public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        if (button == 0) {
          Resources.GUISOUNDS[0].play(KnightsAndArrows.VOLUME);
          OptionsScreen.this.state = 0;
        }
        
        super.touchUp(event, x, y, pointer, button);
      }
    });
    this.stage2.addActor(backButton);
    
    String musicText = !music ? "Music: OFF" : "Music: ON";
    final TextButton musicOption = new TextButton(musicText, style);
    musicOption.setSize(256.0F, 96.0F);
    musicOption.setPosition(Gdx.graphics.getWidth() / 2 - 128, Gdx.graphics.getHeight() / 2);
    musicOption.addListener(new InputListener()
    {
      public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) { return true; }
      
      public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        if (button == 0) {
          Resources.GUISOUNDS[0].play(KnightsAndArrows.VOLUME);
          OptionsScreen.music = !OptionsScreen.music;
          String textMusic = !OptionsScreen.music ? "Music: OFF" : "Music: ON";
          musicOption.setText(textMusic);
          
          if (OptionsScreen.music)
          {
            Resources.MUSIC[0].play();
          }
          else {
            Resources.MUSIC[0].pause();
          }
        }
        


        super.touchUp(event, x, y, pointer, button);
      }
    });
    this.stage3.addActor(musicOption);
    
    final TextButton soundOption = new TextButton(this.soundState, style);
    soundOption.setSize(256.0F, 96.0F);
    soundOption.setPosition(Gdx.graphics.getWidth() / 2 - 128, Gdx.graphics.getHeight() / 2 - 120);
    soundOption.addListener(new InputListener()
    {
      public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) { return true; }
      
      public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        if (button == 0) {
          Resources.GUISOUNDS[0].play(KnightsAndArrows.VOLUME);
          OptionsScreen.sounds = !OptionsScreen.sounds;
          String shad = OptionsScreen.sounds ? "Sounds: ON" : "Sounds: OFF";
          soundOption.setText(shad);
          OptionsScreen.this.prevVol = KnightsAndArrows.VOLUME;
          if (OptionsScreen.sounds)
          {
            if (OptionsScreen.this.prevVol > 0.0F)
            {
              KnightsAndArrows.VOLUME = OptionsScreen.this.prevVol;
            }
            else {
              KnightsAndArrows.VOLUME = 1.0F;
            }
          }
          else {
            System.out.println(OptionsScreen.this.prevVol);
            KnightsAndArrows.VOLUME = 0.0F;
          }
        }
        

        super.touchUp(event, x, y, pointer, button);
      }
    });
    this.stage3.addActor(soundOption);
    


    TextButton baButton = new TextButton("Back", style);
    baButton.setSize(256.0F, 96.0F);
    baButton.setPosition(Gdx.graphics.getWidth() / 2 - 128, Gdx.graphics.getHeight() / 2 - 272);
    baButton.addListener(new InputListener()
    {
      public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) { return true; }
      
      public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        if (button == 0) {
          Resources.GUISOUNDS[0].play(KnightsAndArrows.VOLUME);
          OptionsScreen.this.state = 0;
        }
        
        super.touchUp(event, x, y, pointer, button);
      }
    });
    this.stage3.addActor(baButton);
  }
  







  public void show()
  {
    this.batch = new SpriteBatch();
    this.stage1 = new Stage();
    this.stage2 = new Stage();
    this.stage3 = new Stage();
    this.atlas = new com.badlogic.gdx.graphics.g2d.TextureAtlas(Resources.internal("images/buttons.pack"));
    this.skin = new Skin();
    this.skin.addRegions(this.atlas);
  }
  
  public void hide() {
    dispose();
    this.stage1.dispose();
    this.stage2.dispose();
    this.stage3.dispose();
  }
  
  public void pause() {}
  
  public void resume() {}
  
  public void dispose() {
    this.stage1.dispose();
    this.stage2.dispose();
    this.stage3.dispose();
    this.batch.dispose();
    this.atlas.dispose();
  }
}
