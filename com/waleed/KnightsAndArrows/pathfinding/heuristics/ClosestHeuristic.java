package com.waleed.KnightsAndArrows.pathfinding.heuristics;

import com.waleed.KnightsAndArrows.pathfinding.AStarHeuristic;
import com.waleed.KnightsAndArrows.pathfinding.Mover;
import com.waleed.KnightsAndArrows.pathfinding.TileBasedMap;

public class ClosestHeuristic implements AStarHeuristic {

   public float getCost(TileBasedMap map, Mover mover, int x, int y, int tx, int ty) {
      float dx = (float)(tx - x);
      float dy = (float)(ty - y);
      float result = (float)Math.sqrt((double)(dx * dx + dy * dy));
      return result;
   }
}
