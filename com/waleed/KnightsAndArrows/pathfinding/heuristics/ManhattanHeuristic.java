package com.waleed.KnightsAndArrows.pathfinding.heuristics;

import com.waleed.KnightsAndArrows.pathfinding.AStarHeuristic;
import com.waleed.KnightsAndArrows.pathfinding.Mover;
import com.waleed.KnightsAndArrows.pathfinding.TileBasedMap;

public class ManhattanHeuristic implements AStarHeuristic {

   private int minimumCost;


   public ManhattanHeuristic(int minimumCost) {
      this.minimumCost = minimumCost;
   }

   public float getCost(TileBasedMap map, Mover mover, int x, int y, int tx, int ty) {
      return (float)(this.minimumCost * (Math.abs(x - tx) + Math.abs(y - ty)));
   }
}
