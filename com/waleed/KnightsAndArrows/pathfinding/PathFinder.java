package com.waleed.KnightsAndArrows.pathfinding;

import com.waleed.KnightsAndArrows.pathfinding.Mover;
import com.waleed.KnightsAndArrows.pathfinding.Path;

public interface PathFinder {

   Path findPath(Mover var1, int var2, int var3, int var4, int var5);
}
