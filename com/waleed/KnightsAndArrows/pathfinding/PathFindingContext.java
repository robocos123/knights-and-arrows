package com.waleed.KnightsAndArrows.pathfinding;

import com.waleed.KnightsAndArrows.pathfinding.Mover;

public interface PathFindingContext {

   Mover getMover();

   int getSourceX();

   int getSourceY();

   int getSearchDistance();
}
