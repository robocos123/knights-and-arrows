package com.waleed.KnightsAndArrows.pathfinding;

import com.waleed.KnightsAndArrows.pathfinding.Mover;
import com.waleed.KnightsAndArrows.pathfinding.TileBasedMap;

public interface AStarHeuristic {

   float getCost(TileBasedMap var1, Mover var2, int var3, int var4, int var5, int var6);
}
