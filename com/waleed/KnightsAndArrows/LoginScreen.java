package com.waleed.KnightsAndArrows;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.waleed.KnightsAndArrows.util.LoginThread;
import com.waleed.KnightsAndArrows.util.RegisterThread;
import java.io.IOException;
import java.io.PrintStream;
import java.net.MalformedURLException;
import java.net.URL;

public class LoginScreen implements com.badlogic.gdx.Screen
{
  private Stage stage1;
  private Stage stage2;
  private Stage stage3;
  private Stage stage4;
  private Skin skin;
  private TextureAtlas atlas;
  private SpriteBatch batch;
  public byte state = 0;
  public TextField name;
  public TextField password;
  public TextField nameReg;
  public TextField passReg;
  public TextField confirmPass;
  public TextField email;
  TextButton guestButton;
  public static String errorMsg;
  RegisterThread register;
  LoginThread login;
  Thread loginThread;
  Thread registerThread;
  public boolean finished;
  public boolean loginError = false; public boolean registerError = false;
  
  public static LoginScreen INSTANCE;
  
  TextButton.TextButtonStyle style;
  
  String username;
  
  float countdown = 7.0F;
  
  public LoginScreen()
  {
    Resources.load();
  }
  
  public void render(float delta) {
    Gdx.gl.glClear(16384);
    Gdx.gl.glClearColor(0.0F, 0.0F, 0.0F, 0.0F);
    switch (this.state) {
    case 0: 
      Gdx.input.setInputProcessor(this.stage1);
      this.stage1.act(delta);
      this.stage1.draw();
      

      this.batch.begin();
      
      Resources.FONT.setColor(com.badlogic.gdx.graphics.Color.WHITE);
      
      BitmapFont.TextBounds tb = Resources.FONT.getBounds("Login to your account");
      Resources.FONT.setScale(2.0F);
      Resources.FONT.draw(this.batch, "Login to your account", Gdx.graphics.getWidth() / 2 - tb.width / 2.0F - 100.0F, Gdx.graphics.getHeight() / 2 + tb.height + 200.0F);
      Resources.FONT.setScale(2.0F);
      this.batch.end();
      break;
    case 1: 
      Gdx.input.setInputProcessor(this.stage2);
      this.stage2.act(delta);
      this.stage2.draw();
      this.batch.begin();
      
      BitmapFont.TextBounds tb4 = Resources.FONT.getBounds("Create a new account");
      Resources.FONT.setScale(2.0F);
      Resources.FONT.draw(this.batch, "Create a new account", Gdx.graphics.getWidth() / 2 - tb4.width / 2.0F - 100.0F, Gdx.graphics.getHeight() / 2 + tb4.height + 200.0F);
      Resources.FONT.setScale(2.0F);
      
      this.batch.end();
      break;
    

    case 2: 
      Gdx.input.setInputProcessor(this.stage3);
      TextButton backButton = new TextButton("Back", this.style);
      this.stage3.act(delta);
      this.stage3.draw();
      this.batch.begin();
      BitmapFont.TextBounds tb3 = Resources.FONT.getBounds("Error: " + this.login.getErrorMessage());
      Resources.FONT.setScale(3.0F);
      if (this.login.isError())
      {
        Resources.FONT.setScale(2.0F);
        Resources.FONT.setColor(1.0F, 0.0F, 0.0F, 0.5F);
        Resources.FONT.draw(this.batch, "Error: " + this.login.getErrorMessage(), Gdx.graphics.getWidth() / 2.0F - 605.0F, Gdx.graphics.getHeight() / 2 + tb3.height + 200.0F);
        
        Resources.FONT.setColor(1.0F, 1.0F, 1.0F, 1.0F);
        Resources.FONT.draw(this.batch, "Press ESC to return", Gdx.graphics.getWidth() / 2.0F - 605.0F, Gdx.graphics.getHeight() / 2 + tb3.height + 120.0F);
        
        if (Gdx.input.isKeyPressed(131))
        {
          this.state = 0;
        }
      }
      else if ((!this.login.isError()) && (!this.login.isFinished()))
      {
        Resources.FONT.draw(this.batch, "Logging In..", Gdx.graphics.getWidth() / 2 - tb3.width, Gdx.graphics.getHeight() / 2 + tb3.height);
        Resources.FONT.setScale(2.0F);
      }
      
      this.batch.end();
      
      if ((!this.login.isError()) && (this.login.isFinished()))
      {
        System.out.println(this.username == this.login.userName);
        KnightsAndArrows.INSTANCE.setScreen(new SplashScreen(this.username));
      }
      
      break;
    case 3: 
      Gdx.input.setInputProcessor(this.stage4);
      this.stage4.act(delta);
      this.stage4.draw();
      this.batch.begin();
      if (this.register.isError())
      {
        BitmapFont.TextBounds error = Resources.FONT.getBounds("Setting up your account..");
        
        Resources.FONT.setScale(2.0F);
        Resources.FONT.setColor(1.0F, 0.0F, 0.0F, 0.5F);
        Resources.FONT.draw(this.batch, "Error: " + this.register.getErrorMessage(), Gdx.graphics.getWidth() / 2.0F - 605.0F, Gdx.graphics.getHeight() / 2 + error.height + 200.0F);
        Resources.FONT.setScale(2.0F);
        Resources.FONT.setColor(1.0F, 1.0F, 1.0F, 1.0F);
        Resources.FONT.draw(this.batch, "Press ESC to return", Gdx.graphics.getWidth() / 2.0F - 605.0F, Gdx.graphics.getHeight() / 2 + error.height + 120.0F);
        
        if (Gdx.input.isKeyPressed(131))
        {
          this.state = 1;
        }
      }
      else if ((this.register.isFinished()) && (!this.register.isError()))
      {
        BitmapFont.TextBounds setup = Resources.FONT.getBounds("Setting up your account..");
        Resources.FONT.setScale(2.0F);
        Resources.FONT.setColor(0.0F, 1.0F, 0.0F, 0.5F);
        Resources.FONT.draw(this.batch, this.register.userName + ": Your account setup is complete!", Gdx.graphics.getWidth() / 2 - 7.0F - setup.width / 2.0F, Gdx.graphics.getHeight() / 2 + setup.height);
        Resources.FONT.setColor(1.0F, 1.0F, 1.0F, 1.0F);
        Resources.FONT.draw(this.batch, "Press ESC to return", Gdx.graphics.getWidth() / 2 + 128.0F - setup.width / 2.0F, Gdx.graphics.getHeight() / 2 + setup.height - 110.0F);
        
        Resources.FONT.setScale(2.0F);
        

        if (Gdx.input.isKeyPressed(131))
        {
          this.state = 0;
        }
      }
      else {
        BitmapFont.TextBounds setup = Resources.FONT.getBounds("Setting up your account..");
        Resources.FONT.setScale(3.0F);
        Resources.FONT.draw(this.batch, "Setting up your account..", Gdx.graphics.getWidth() / 2 - 7.0F - setup.width / 2.0F, Gdx.graphics.getHeight() / 2 + setup.height);
        Resources.FONT.setScale(2.0F);
      }
      
      this.batch.end();
      break;
    
    default: 
      this.state = 0;
    }
  }
  
  private static boolean isLoginAvaliable()
  {
    try
    {
      URL url = new URL(KnightsAndArrows.INSTANCE.websiteURL);
      java.net.URLConnection conn = url.openConnection();
      conn.connect();
      System.out.println("Login Server is stable");
      return true;
    } catch (MalformedURLException e) {
      String error = "Login Server is unstable" + e.toString();
      System.out.println(error);
      setError(error);
      return false;
    } catch (IOException e) {
      String error = "Login Server is unstable: " + e.toString();
      System.out.println(error);
      setError(error); }
    return false;
  }
  


  public static void setError(String error)
  {
    errorMsg = error;
  }
  
  public static String getError()
  {
    return errorMsg;
  }
  
  public void beginLogin()
  {
    if (isLoginAvaliable())
    {
      System.out.println("Login is avaliable");
      this.username = this.name.getText();
      String password = this.password.getText();
      System.out.println("$USERNAME: " + this.username);
      System.out.println("$PASSWORD: " + password);
      this.login = new LoginThread(this.username, password);
      this.loginThread = new Thread(this.login);
      this.loginThread.start();
      

      this.state = 2;
    }
    else
    {
      System.out.println("Login is not avaliable");
      this.state = 2;
    }
  }
  


  public void beginRegister()
  {
    if (isLoginAvaliable())
    {
      String username = this.nameReg.getText();
      String password = this.passReg.getText();
      String confirmPassword = this.confirmPass.getText();
      System.out.println("$USERNAME: " + username);
      System.out.println("$PASSWORD: " + password);
      System.out.println("$CONFIRM_PASSWORD: " + confirmPassword);
      this.register = new RegisterThread(username, password, confirmPassword);
      this.registerThread = new Thread(this.register);
      this.registerThread.start();
      
      this.state = 3;
    }
    else
    {
      this.state = 2;
    }
  }
  






  public void resize(int width, int height)
  {
    Resources.FONT.setScale(1.0F);
    this.stage1 = new Stage(width, height, true, this.batch);
    this.stage2 = new Stage(width, height, true, this.batch);
    com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle lstyle = new com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle();
    lstyle.font = Resources.FONT;
    
    Label label2 = new Label("Username: ", lstyle);
    label2.setPosition(this.stage1.getWidth() / 2.0F - 310.0F, this.stage1.getHeight() / 2.0F - 7.0F + 30.0F);
    this.stage1.addActor(label2);
    

    com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldStyle fieldStyle = new com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldStyle();
    fieldStyle.background = Resources.BUTTONS.getDrawable("field_bg");
    fieldStyle.disabledBackground = Resources.BUTTONS.getDrawable("field_bg");
    fieldStyle.focusedBackground = Resources.BUTTONS.getDrawable("field_focus");
    fieldStyle.cursor = Resources.BUTTONS.getDrawable("cursor");
    fieldStyle.font = Resources.FONT;
    fieldStyle.fontColor = com.badlogic.gdx.graphics.Color.WHITE;
    this.name = new TextField("", fieldStyle);
    this.name.setCursorPosition(1);
    this.name.setMaxLength(16);
    this.name.setBlinkTime(0.5F);
    this.name.setPosition(this.stage1.getWidth() / 2.0F - 135.0F, this.stage1.getHeight() / 2.0F - 7.0F + 10.0F);
    this.name.setSize(192.0F, 52.0F);
    

    this.name.addListener(new InputListener() {
      public boolean keyTyped(InputEvent event, char character) {
        TextField field = (TextField)event.getListenerActor();
        if (field.getText().length() == 0) {
          field.setText("");
          field.setCursorPosition(1);
        }
        
        return super.keyTyped(event, character);
      }
      
      public boolean keyUp(InputEvent event, int keycode) { if (keycode == 66) {
          Resources.GUISOUNDS[0].play();
          LoginScreen.this.beginLogin();
        }
        
        return super.keyUp(event, keycode);
      }
    });
    this.stage1.addActor(this.name);
    




    Label label3 = new Label("Password: ", lstyle);
    label3.setPosition(this.stage1.getWidth() / 2.0F - 310.0F, this.stage1.getHeight() / 2.0F - 51.0F);
    this.stage1.addActor(label3);
    
    this.password = new TextField("", fieldStyle);
    this.password.setPasswordMode(true);
    this.password.setPasswordCharacter('*');
    this.password.setCursorPosition(2);
    this.password.setMaxLength(20);
    this.password.setBlinkTime(0.5F);
    this.password.setPosition(this.stage1.getWidth() / 2.0F - 135.0F, this.stage1.getHeight() / 2.0F - 70.0F);
    this.password.setSize(192.0F, 52.0F);
    

    this.password.addListener(new InputListener() {
      public boolean keyTyped(InputEvent event, char character) {
        TextField field = (TextField)event.getListenerActor();
        if (field.getText().length() == 0) {
          field.setText("");
          field.setCursorPosition(1);
        }
        
        return super.keyTyped(event, character);
      }
      
      public boolean keyUp(InputEvent event, int keycode) { if (keycode == 66) {
          Resources.GUISOUNDS[0].play();
          LoginScreen.this.beginLogin();
        }
        
        return super.keyUp(event, keycode);
      }
    });
    this.stage1.addActor(this.password);
    

    this.style = new TextButton.TextButtonStyle();
    this.style.up = this.skin.getDrawable("button_off");
    this.style.over = this.skin.getDrawable("button_on");
    this.style.down = this.skin.getDrawable("button_active");
    this.style.font = Resources.FONT;
    this.style.font.setScale(2.0F);
    TextButton startButton = new TextButton("Login", this.style);
    startButton.setSize(256.0F, 96.0F);
    startButton.setPosition(Gdx.graphics.getWidth() / 2 - 256, Gdx.graphics.getHeight() / 2 - 200);
    startButton.addListener(new InputListener()
    {
      public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) { return true; }
      
      public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        if (button == 0) {
          Resources.GUISOUNDS[0].play(KnightsAndArrows.VOLUME);
          LoginScreen.this.beginLogin();
        }
        
        super.touchUp(event, x, y, pointer, button);
      }
    });
    this.stage1.addActor(startButton);
    
    TextButton exitButton = new TextButton("Don't have an account?", this.style);
    exitButton.setSize(420.0F, 96.0F);
    exitButton.setPosition(Gdx.graphics.getWidth() / 2 - 256, Gdx.graphics.getHeight() / 2 - 360);
    exitButton.addListener(new InputListener()
    {
      public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) { return true; }
      
      public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        if (button == 0) {
          Resources.GUISOUNDS[0].play(KnightsAndArrows.VOLUME);
          LoginScreen.this.state = 1;
        }
        
        super.touchUp(event, x, y, pointer, button);
      }
    });
    this.stage1.addActor(exitButton);
    

    this.guestButton = new TextButton("Offline Mode", this.style);
    this.guestButton.setSize(300.0F, 96.0F);
    this.guestButton.setPosition(Gdx.graphics.getWidth() / 2 + 256, Gdx.graphics.getHeight() / 2 - 360);
    this.guestButton.addListener(new InputListener()
    {
      public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) { return true; }
      
      public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        if (button == 0) {
          Resources.GUISOUNDS[0].play(KnightsAndArrows.VOLUME);
          KnightsAndArrows.INSTANCE.setScreen(new MenuScreen("Guest", false));
        }
        
        super.touchUp(event, x, y, pointer, button);
      }
      
    });
    this.stage1.addActor(this.guestButton);
    






    Label label4 = new Label("Username: ", lstyle);
    label4.setPosition(this.stage2.getWidth() / 2.0F - 310.0F, this.stage2.getHeight() / 2.0F - 7.0F + 70.0F);
    this.stage2.addActor(label4);
    

    this.nameReg = new TextField("", fieldStyle);
    this.nameReg.setCursorPosition(1);
    this.nameReg.setMaxLength(16);
    this.nameReg.setBlinkTime(0.5F);
    this.nameReg.setPosition(this.stage2.getWidth() / 2.0F - 135.0F, this.stage2.getHeight() / 2.0F - 7.0F + 50.0F);
    this.nameReg.setSize(192.0F, 52.0F);
    

    this.nameReg.addListener(new InputListener() {
      public boolean keyTyped(InputEvent event, char character) {
        TextField field = (TextField)event.getListenerActor();
        if (field.getText().length() == 0) {
          field.setText("");
          field.setCursorPosition(1);
        }
        
        return super.keyTyped(event, character);
      }
      
      public boolean keyUp(InputEvent event, int keycode) { if (keycode == 66) {
          Resources.GUISOUNDS[0].play();
          LoginScreen.this.beginRegister();
        }
        
        return super.keyUp(event, keycode);
      }
    });
    this.stage2.addActor(this.nameReg);
    




    Label label5 = new Label("Password: ", lstyle);
    label5.setPosition(this.stage2.getWidth() / 2.0F - 310.0F, this.stage2.getHeight() / 2.0F - 7.0F + 10.0F);
    this.stage2.addActor(label5);
    
    this.passReg = new TextField("", fieldStyle);
    this.passReg.setPasswordMode(true);
    this.passReg.setPasswordCharacter('*');
    this.passReg.setCursorPosition(2);
    this.passReg.setMaxLength(20);
    this.passReg.setBlinkTime(0.5F);
    this.passReg.setPosition(this.stage2.getWidth() / 2.0F - 135.0F, this.stage2.getHeight() / 2.0F - 7.0F - 10.0F);
    this.passReg.setSize(192.0F, 52.0F);
    

    this.passReg.addListener(new InputListener() {
      public boolean keyTyped(InputEvent event, char character) {
        TextField field = (TextField)event.getListenerActor();
        if (field.getText().length() == 0) {
          field.setText("");
          field.setCursorPosition(1);
        }
        
        return super.keyTyped(event, character);
      }
      
      public boolean keyUp(InputEvent event, int keycode) { if (keycode == 66) {
          Resources.GUISOUNDS[0].play();
          LoginScreen.this.beginRegister();
        }
        
        return super.keyUp(event, keycode);
      }
    });
    this.stage2.addActor(this.passReg);
    

    Label label6 = new Label("Confirm Password: ", lstyle);
    label6.setPosition(this.stage2.getWidth() / 2.0F - 420.0F - 25.0F, this.stage2.getHeight() / 2.0F - 7.0F - 50.0F);
    this.stage2.addActor(label6);
    
    this.confirmPass = new TextField("", fieldStyle);
    this.confirmPass.setPasswordMode(true);
    this.confirmPass.setPasswordCharacter('*');
    this.confirmPass.setCursorPosition(2);
    this.confirmPass.setMaxLength(20);
    this.confirmPass.setBlinkTime(0.5F);
    this.confirmPass.setPosition(this.stage2.getWidth() / 2.0F - 135.0F, this.stage2.getHeight() / 2.0F - 7.0F - 70.0F);
    this.confirmPass.setSize(192.0F, 52.0F);
    

    this.confirmPass.addListener(new InputListener() {
      public boolean keyTyped(InputEvent event, char character) {
        TextField field = (TextField)event.getListenerActor();
        if (field.getText().length() == 0) {
          field.setText("");
          field.setCursorPosition(1);
        }
        
        return super.keyTyped(event, character);
      }
      
      public boolean keyUp(InputEvent event, int keycode) { if (keycode == 66) {
          Resources.GUISOUNDS[0].play();
          LoginScreen.this.beginRegister();
        }
        
        return super.keyUp(event, keycode);
      }
    });
    this.stage2.addActor(this.confirmPass);
    


    this.style = new TextButton.TextButtonStyle();
    this.style.up = this.skin.getDrawable("button_off");
    this.style.over = this.skin.getDrawable("button_on");
    this.style.down = this.skin.getDrawable("button_active");
    this.style.font = Resources.FONT;
    this.style.font.setScale(2.0F);
    TextButton registerButton = new TextButton("Register", this.style);
    registerButton.setSize(256.0F, 96.0F);
    registerButton.setPosition(Gdx.graphics.getWidth() / 2 - 256, Gdx.graphics.getHeight() / 2 - 200);
    registerButton.addListener(new InputListener()
    {
      public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) { return true; }
      
      public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        if (button == 0) {
          Resources.GUISOUNDS[0].play(KnightsAndArrows.VOLUME);
          LoginScreen.this.beginRegister();
        }
        
        super.touchUp(event, x, y, pointer, button);
      }
    });
    this.stage2.addActor(registerButton);
    

    TextButton backLoginButton = new TextButton("Back to Login", this.style);
    backLoginButton.setSize(256.0F, 96.0F);
    backLoginButton.setPosition(Gdx.graphics.getWidth() / 2 - 256, Gdx.graphics.getHeight() / 2 - 340);
    backLoginButton.addListener(new InputListener()
    {
      public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) { return true; }
      
      public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        if (button == 0) {
          Resources.GUISOUNDS[0].play(KnightsAndArrows.VOLUME);
          LoginScreen.this.state = 0;
        }
        
        super.touchUp(event, x, y, pointer, button);
      }
    });
    this.stage2.addActor(backLoginButton);
  }
  
  public void show()
  {
    this.batch = new SpriteBatch();
    this.stage1 = new Stage();
    this.stage2 = new Stage();
    this.stage3 = new Stage();
    this.stage4 = new Stage();
    this.atlas = new TextureAtlas(Resources.internal("images/buttons.pack"));
    this.skin = new Skin();
    this.skin.addRegions(this.atlas);
  }
  
  public void hide() {
    dispose();
  }
  
  public void pause() {}
  
  public void resume() {}
  
  public void dispose() {
    this.stage1.dispose();
    this.stage2.dispose();
    this.skin.dispose();
    this.batch.dispose();
    this.atlas.dispose();
  }
}
