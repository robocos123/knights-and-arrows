package com.waleed.KnightsAndArrows;

public class GameKeys
{
  private static boolean[] keys = new boolean[8];
  private static boolean[] pkeys = new boolean[8];
  
  private static final int NUM_KEYS = 8;
  
  public static final int UP = 0;
  
  public static final int LEFT = 1;
  public static final int DOWN = 2;
  public static final int RIGHT = 3;
  public static final int ENTER = 4;
  public static final int ESCAPE = 5;
  public static final int SPACE = 6;
  public static final int SHIFT = 7;
  public static final int E = 8;
  
  public static void update()
  {
    for (int i = 0; i < 8; i++) {
      pkeys[i] = keys[i];
    }
  }
  
  public static void setKey(int k, boolean b)
  {
    keys[k] = b;
  }
  
  public static boolean isDown(int k) {
    return keys[k];
  }
  
  public static boolean isPressed(int k) {
    return (keys[k] != false) && (pkeys[k] == false);
  }
}
