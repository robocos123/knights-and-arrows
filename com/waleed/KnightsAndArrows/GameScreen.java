package com.waleed.KnightsAndArrows;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GLCommon;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Matrix4;
import com.waleed.KnightsAndArrows.core.Level;
import com.waleed.KnightsAndArrows.core.gui.GuiHandler;
import com.waleed.KnightsAndArrows.util.Camera;
import org.lwjgl.opengl.Display;

public class GameScreen implements Screen
{
  private Level level;
  private SpriteBatch batch;
  private Matrix4 screenView;
  private String path;
  private GuiHandler gui;
  private boolean paused;
  private boolean debug = true;
  
  public GameScreen(String path)
  {
    this.path = path;
  }
  
  public void render(float delta) {
    if (Input.isKeyPressed(131)) {
      if (KnightsAndArrows.isOffline()) {
        KnightsAndArrows.INSTANCE.setScreen(new MenuScreen("Guest", false));
      } else {
        KnightsAndArrows.INSTANCE.setScreen(new MenuScreen(SplashScreen.username, true));
      }
    }
    
    if (!this.paused) {
      this.paused = ((Input.isKeyPressed(44)) || (!Display.isActive()));
    } else {
      this.paused = (!Input.isKeyPressed(44));
    }
    
    if (this.debug)
    {
      if (Input.isKeyDown(32))
      {
        KnightsAndArrows.INSTANCE.setScreen(new EndScreen(true, 696969, 420.0F, KnightsAndArrows.INSTANCE.username));
        Resources.SOUNDS[10].stop();
      }
    }
    
    if (Input.isKeyPressed(81)) {
      KnightsAndArrows.VOLUME += 0.1F;
      KnightsAndArrows.VOLUME = MathUtils.clamp(KnightsAndArrows.VOLUME, 0.0F, 1.0F);
      Resources.GUISOUNDS[1].play(KnightsAndArrows.VOLUME);
    } else if (Input.isKeyPressed(69)) {
      KnightsAndArrows.VOLUME -= 0.1F;
      KnightsAndArrows.VOLUME = MathUtils.clamp(KnightsAndArrows.VOLUME, 0.0F, 1.0F);
      Resources.GUISOUNDS[1].play(KnightsAndArrows.VOLUME);
    }
    
    Gdx.gl.glClear(16384);
    Gdx.gl.glClearColor(0.0F, 0.0F, 0.0F, 0.0F);
    Input.update();
    Camera camera = this.level.getCamera();
    if (!this.paused) {
      if (Input.isButtonDown(2)) {
        camera.translate(Input.getMouseDX(), Input.getMouseDY());
      }
      

      camera.clampTo(0.0F, 0.0F, this.level.getWidthInTiles() << 5, this.level.getHeightInTiles() << 5);
      camera.transform(delta);
      camera.update();
    }
    
    this.level.updateAndRender(delta, this.screenView, this.batch, this.paused);
    this.batch.setProjectionMatrix(this.screenView);
    this.gui.updateAndRender(delta, this.batch, camera, this.paused);
    if (this.paused) {
      this.batch.begin();
      this.batch.draw(Resources.GUI, 0.0F, 0.0F, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), 127, 127, 1, 1, false, false);
      Resources.FONT.setScale(4.0F);
      String text = "Paused";
      BitmapFont.TextBounds tb = Resources.FONT.getBounds(text);
      Resources.FONT.draw(this.batch, text, Gdx.graphics.getWidth() / 2 - tb.width / 2.0F, Gdx.graphics.getHeight() / 2 + tb.height);
      Resources.FONT.setScale(1.0F);
      this.batch.end();
    }
  }
  
  public void resize(int width, int height)
  {
    this.gui.resize(width, height);
    this.level.resize(width, height);
    this.screenView.setToOrtho2D(0.0F, 0.0F, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
  }
  
  public void show() {
    Resources.FONT.setScale(1.0F);
    this.level = new Level(this.path);
    this.batch = new SpriteBatch();
    this.screenView = new Matrix4();
    this.screenView.setToOrtho2D(0.0F, 0.0F, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
    this.gui = new GuiHandler(this.level);
  }
  
  public void hide() {
    dispose();
  }
  
  public void pause() {
    this.paused = true;
  }
  
  public void resume() {
    this.paused = false;
  }
  
  public void dispose() {
    this.level.dispose();
    this.batch.dispose();
    this.gui.dispose();
  }
}
