package com.waleed.KnightsAndArrows;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.graphics.GLCommon;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

public class SplashScreen implements com.badlogic.gdx.Screen
{
  private SpriteBatch batch = new SpriteBatch();
  private byte state = 0;
  private float countdown = 7.0F;
  
  public String onlineVersion;
  public boolean fullScreen;
  static String username;
  
  public SplashScreen(String username)
  {
    username = username;
  }
  
  public void render(float delta) {
    Gdx.gl.glClear(16384);
    Gdx.gl.glClearColor(0.0F, 0.0F, 0.0F, 0.0F);
    this.batch.begin();
    Resources.FONT.setScale(3.0F);
    BitmapFont.TextBounds tb = Resources.FONT.getBounds("Loading...");
    Resources.FONT.draw(this.batch, "Loading...", Gdx.graphics.getWidth() / 2 - tb.width / 2.0F, Gdx.graphics.getHeight() / 2 + tb.height);
    Resources.FONT.setScale(3.0F);
    this.batch.end();
    this.countdown -= delta;
    if (this.state == 1) {
      this.state = ((byte)(this.state + 1));
      Resources.load();
    }
    

    if (this.countdown <= 5.0F) {
      Gdx.gl.glClear(16384);
      Gdx.gl.glClearColor(0.0F, 0.0F, 0.0F, 0.0F);
      this.batch.begin();
      BitmapFont.TextBounds tb2 = Resources.FONT.getBounds("Checking for updates...");
      Resources.FONT.draw(this.batch, "Checking for updates...", Gdx.graphics.getWidth() / 2 - tb2.width / 2.0F, Gdx.graphics.getHeight() / 2 + tb2.height);
      Resources.FONT.setScale(1.0F);
      this.batch.end();
      
      if (this.countdown <= 0.0F) {
        try
        {
          URL url = new URL("http://biomes.host-ed.me/ka/version.txt");
          BufferedReader input = new BufferedReader(new InputStreamReader(url.openStream()));
          if ((this.onlineVersion = input.readLine()) != null)
          {
            double x = Double.valueOf(this.onlineVersion).doubleValue();
            double y = Double.valueOf(KnightsAndArrows.version).doubleValue();
            if (x <= y)
            {
              if (KnightsAndArrows.isOffline()) {
                KnightsAndArrows.INSTANCE.setScreen(new MenuScreen("Guest", false));
              } else {
                KnightsAndArrows.INSTANCE.setScreen(new MenuScreen(username, true));
              }
            } else if (x > y)
            {
              KnightsAndArrows.INSTANCE.setScreen(new UpdateScreen());
            }
          }
        }
        catch (Exception e) {
          e.printStackTrace();
          KnightsAndArrows.INSTANCE.setScreen(new MenuScreen("Guest", false));
          KnightsAndArrows.setOffline(true);
        }
      }
    }
    


    this.state = ((byte)(this.state + 1));
  }
  
  public void resize(int width, int height) {}
  
  public void show() {}
  
  public void hide() {
    dispose();
  }
  
  public void pause() {}
  
  public void resume() {}
  
  public void dispose() {
    this.batch.dispose();
  }
}
