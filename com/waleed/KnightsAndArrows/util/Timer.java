package com.waleed.KnightsAndArrows.util;

import com.badlogic.gdx.utils.TimeUtils;

public class Timer
{
  private long start;
  private long secsToWait;
  
  public Timer(long secsToWait)
  {
    this.secsToWait = secsToWait;
  }
  
  public void start()
  {
    this.start = (TimeUtils.millis() / 1000L);
  }
  
  public boolean hasCompleted()
  {
    return TimeUtils.millis() / 1000L - this.start >= this.secsToWait;
  }
}
