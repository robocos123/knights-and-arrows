package com.waleed.KnightsAndArrows.util;

import java.io.BufferedReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

public class RegisterThread implements Runnable
{
  public String userName;
  public String password;
  public String confirmPassword;
  public boolean isError;
  public boolean isFinished;
  public String error;
  
  public RegisterThread(String userName, String password, String confirmPassword)
  {
    this.userName = userName;
    this.password = password;
    this.confirmPassword = confirmPassword;
  }
  

  public void run()
  {
    try
    {
      String data = URLEncoder.encode("fName", "UTF-8") + "=" + URLEncoder.encode(this.userName, "UTF-8");
      data = data + "&" + URLEncoder.encode("lName", "UTF-8") + "=" + URLEncoder.encode(this.password, "UTF-8");
      data = data + "&" + URLEncoder.encode("kName", "UTF-8") + "=" + URLEncoder.encode(this.confirmPassword, "UTF-8");
      
      URL url = new URL("http://biomes.host-ed.me/ka/login/regisAuth.php");
      URLConnection conn = url.openConnection();
      conn.setDoOutput(true);
      conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401");
      OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
      wr.write(data);
      wr.flush();
      
      BufferedReader rd = new BufferedReader(new java.io.InputStreamReader(conn.getInputStream()));
      String line;
      while ((line = rd.readLine()) != null) { 
        System.out.println("$_GET: (" + line + ")");
        
        this.isError = ((line.contains("do not match")) || (line.contains("blank")) || (line.contains("exists")));
        
        if (this.isError)
        {
          this.error = line.toString();
        }
        else {
          this.error = ("The account " + this.userName + " has been created successfully!");
          this.isFinished = true;
        }
      }
      
      wr.close();
      rd.close();
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }
  
  public boolean isError()
  {
    return this.isError;
  }
  
  public boolean isFinished()
  {
    return this.isFinished;
  }
  
  public String getErrorMessage()
  {
    if (isError()) {
      return this.error;
    }
    return null;
  }
}
