package com.waleed.KnightsAndArrows.util;

import com.waleed.KnightsAndArrows.KnightsAndArrows;
import com.waleed.KnightsAndArrows.UpdateScreen;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;

public class Downloader implements Runnable
{
  String bin = KnightsAndArrows.class.getProtectionDomain().getCodeSource()
    .getLocation().getPath();
  
  String decodedPath;
  
  private File jar;
  
  public float percent;
  public boolean isFinished = false;
  
  public UpdateScreen parent = new UpdateScreen();
  
  public boolean isError = false;
  
  boolean existedBefore = true;
  
  public void run() {
    try {
      this.decodedPath = URLDecoder.decode(this.bin, "UTF-8");
    }
    catch (UnsupportedEncodingException e) {
      e.printStackTrace();
    }
    System.out.println(this.decodedPath);
    this.jar = new File(this.decodedPath + "/KA.jar");
    downloadJAR();
  }
  
  private void downloadJAR()
  {
    File file = new File(this.jar.toString());
    String JarURL = new String(KnightsAndArrows.INSTANCE.downloadURL);
    
    if (!file.exists())
      this.existedBefore = false;
    try {
      System.out.println("File is created");
      file.createNewFile();
    }
    catch (IOException e1) {
      e1.printStackTrace();
    }
    
    try
    {
      URL url = new URL(JarURL);
      
      URLConnection urlCon = url.openConnection();
      InputStream is = urlCon.getInputStream();
      FileOutputStream fos = new FileOutputStream(file);
      
      int filesize = urlCon.getContentLength();
      float totalDataRead = 0.0F;
      
      System.out.println(urlCon.getContentType());
      
      byte[] buffer = new byte[filesize];
      for (int bytesRead = is.read(buffer); bytesRead > 0; bytesRead = is
            .read(buffer)) {
        totalDataRead += bytesRead;
        fos.write(buffer, 0, bytesRead);
        float Percent = totalDataRead * 100.0F / filesize;
        this.percent = Percent;
      }
      
      is.close();
      fos.close();
      
      this.isFinished = true;
      
      System.out.println("Finished the download");

    }
    catch (Exception e)
    {
      e.printStackTrace();
      if (!this.existedBefore) {
        file.delete();
      }
      this.isError = true;
      this.isFinished = false;
    }
  }
  



  public boolean isError()
  {
    return this.isError;
  }
}
