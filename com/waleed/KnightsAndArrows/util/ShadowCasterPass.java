package com.waleed.KnightsAndArrows.util;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public abstract interface ShadowCasterPass
{
  public abstract void render(SpriteBatch paramSpriteBatch, Light paramLight);
}
