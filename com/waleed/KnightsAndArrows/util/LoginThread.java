package com.waleed.KnightsAndArrows.util;

import java.io.BufferedReader;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

public class LoginThread implements Runnable
{
  String error;
  boolean finished = false;
  
  public String userName;
  String password;
  
  public LoginThread(String userName, String password)
  {
    this.userName = userName;
    this.password = password;
  }
  

  public void run()
  {
    try
    {
      String data = URLEncoder.encode("fName", "UTF-8") + "=" + URLEncoder.encode(this.userName, "UTF-8");
      data = data + "&" + URLEncoder.encode("lName", "UTF-8") + "=" + URLEncoder.encode(this.password, "UTF-8");
      

      URL url = new URL("http://biomes.host-ed.me/ka/login/auth.php");
      
      URLConnection conn = url.openConnection();
      conn.setDoOutput(true);
      conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401");
      OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
      wr.write(data);
      wr.flush();
      

      BufferedReader rd = new BufferedReader(new java.io.InputStreamReader(conn.getInputStream()));
      String line;
      while ((line = rd.readLine()) != null) { 
        System.out.println("$_GET: (" + line + ")");
        
        if ((line.contains("Failed")) || (line.contains("incorrect")))
        {
          System.err.println("$_GET: (" + line + ")");
          this.error = line;
        }
        else {
          this.error = null;
          this.finished = true;
        }
      }
      
      wr.close();
      rd.close();
    } catch (Exception e) {
      e.printStackTrace();
      this.error = e.toString();
    }
  }
  

  public boolean isError()
  {
    if (this.error == null) {
      return false;
    }
    return true;
  }
  
  public boolean isFinished()
  {
    return this.finished;
  }
  
  public String getErrorMessage()
  {
    if (isError()) {
      return this.error;
    }
    return null;
  }
}
