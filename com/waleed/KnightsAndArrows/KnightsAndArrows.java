package com.waleed.KnightsAndArrows;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.Files.FileType;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import java.awt.Dimension;
import java.awt.Toolkit;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import org.lwjgl.opengl.ContextCapabilities;
import org.lwjgl.opengl.GLContext;

public class KnightsAndArrows
  extends Game
{
  public static Input INPUT = new Input();
  public static KnightsAndArrows INSTANCE = new KnightsAndArrows();
  public static float VOLUME = 1.0F;
  
  public static boolean fullScreen;
  public static String version = "0.11";
  
  public static boolean isOffline = false;
  
  public String downloadURL = "https://dl.dropboxusercontent.com/s/5bdowuarngprak2/KA.jar?dl=1&token_hash=AAFrRuqBLKPovDKUaPILdeLlBcSimV5tgbfXWksfba4Csw";
  public String websiteURL = "http://biomes.host-ed.me";
  public String username;
  
  public void create()
  {
    Texture.setEnforcePotImages(false);
    
    if (!GLContext.getCapabilities().OpenGL20) {
      JOptionPane.showMessageDialog(new JFrame(), 
        "OpenGL20 not available", "ERROR", 0);
      System.exit(0);
    }
    
    Resources.FONT = new BitmapFont(Gdx.files.internal("fonts/basic.fnt"), 
      false);
    com.badlogic.gdx.graphics.glutils.ShaderProgram.pedantic = false;
    


    setScreen(new LoginScreen());
  }
  



  public void dispose()
  {
    super.dispose();
    Resources.dispose();
  }
  
  public static void main(String[] args)
  {
    Toolkit tool = Toolkit.getDefaultToolkit();
    LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
    cfg.title = "Knights & Arrows";
    

    cfg.width = Integer.valueOf((int)tool.getScreenSize().getWidth()).intValue();
    cfg.height = Integer.valueOf((int)tool.getScreenSize().getHeight()).intValue();
    cfg.vSyncEnabled = false;
    cfg.useGL20 = true;
    cfg.fullscreen = true;
    cfg.resizable = true;
    cfg.addIcon("images/icon.png", Files.FileType.Internal);
    new LwjglApplication(INSTANCE, cfg);
  }
  
  public static void setOffline(boolean isOffline)
  {
    isOffline = isOffline;
  }
  
  public static boolean isOffline()
  {
    return isOffline;
  }
}
