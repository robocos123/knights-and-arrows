package com.waleed.KnightsAndArrows;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GLCommon;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Date;
import java.text.SimpleDateFormat;

public class EndScreen implements com.badlogic.gdx.Screen
{
  private Stage stage;
  private Stage stage2;
  private SpriteBatch batch;
  private com.badlogic.gdx.graphics.glutils.ShapeRenderer sr;
  private boolean won;
  private int kills;
  private float time;
  private String username;
  public int state = 0;
  
  private char[] newName;
  
  private int currentChar;
  private BitmapFont font;
  private com.badlogic.gdx.scenes.scene2d.ui.TextField textfieldScore;
  Skin skinSubmit;
  
  public EndScreen(boolean won, int kills, float time, String username)
  {
    this.won = won;
    this.kills = kills;
    this.time = time;
    this.username = username;
  }
  
  public void render(float delta)
  {
    Gdx.gl.glClear(16384);
    Gdx.gl.glClearColor(0.0F, 0.0F, 0.0F, 0.0F);
    switch (this.state)
    {
    case 0: 
      Gdx.input.setInputProcessor(this.stage);
      this.stage.act(delta);
      this.stage.draw();
      Resources.FONT.setScale(4.0F);
      this.batch.begin();
      String player = (this.username != "Guest") || (!isServerUp()) ? this.username + " " : "You ";
      String text = player + "lost!";
      BitmapFont.TextBounds tb = Resources.FONT.getBounds(text);
      Resources.FONT.draw(this.batch, text, 
        Gdx.graphics.getWidth() / 2 - tb.width / 2.0F, 
        Gdx.graphics.getHeight() * 0.8F);
      Resources.FONT.setScale(2.0F);
      String text2 = "Kills: " + this.kills;
      tb = Resources.FONT.getBounds(text2);
      Resources.FONT.draw(this.batch, text2, 
        Gdx.graphics.getWidth() / 2 - tb.width / 2.0F, 
        Gdx.graphics.getHeight() * 0.6F);
      int mili = (int)(this.time * 1000.0F);
      Date date = new Date(mili);
      SimpleDateFormat format = new SimpleDateFormat("mm:ss:SS");
      String dateFormatted = format.format(date);
      String text3 = "Time: " + dateFormatted;
      tb = Resources.FONT.getBounds(text3);
      Resources.FONT.draw(this.batch, text3, 
        Gdx.graphics.getWidth() / 2 - tb.width / 2.0F, 
        Gdx.graphics.getHeight() * 0.5F);
      this.batch.end();
      Resources.FONT.setScale(1.0F);
      break;
    case 1: 
      Gdx.input.setInputProcessor(this.stage2);
      this.stage2.act();
      this.stage2.draw();
      this.batch.begin();
      String name = this.username;
      BitmapFont.TextBounds bound = Resources.FONT.getBounds(name);
      Resources.FONT.draw(this.batch, name, 
        Gdx.graphics.getWidth() / 2 - bound.width / 2.0F, 
        Gdx.graphics.getHeight() * 0.8F);
      this.batch.end();
      Resources.FONT.setScale(1.0F);
    }
  }
  
  public void resize(int width, int height)
  {
    this.stage = new Stage(width, height, true, this.batch);
    this.stage2 = new Stage(width, height, true, this.batch);
    TextButton.TextButtonStyle style = new TextButton.TextButtonStyle();
    style.up = Resources.BUTTONS.getDrawable("button_off");
    style.over = Resources.BUTTONS.getDrawable("button_on");
    style.down = Resources.BUTTONS.getDrawable("button_active");
    style.font = Resources.FONT;
    style.font.setScale(3.0F);
    
    TextButton returnButton = new TextButton("Return", style);
    returnButton.setSize(256.0F, 96.0F);
    returnButton.setPosition(Gdx.graphics.getWidth() / 2 - 128, 
      Gdx.graphics.getHeight() / 2 - 256);
    returnButton.addListener(new InputListener()
    {
      public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        return true;
      }
      
      public void touchUp(InputEvent event, float x, float y, int pointer, int button)
      {
        if (button == 0) {
          Resources.GUISOUNDS[0].play(KnightsAndArrows.VOLUME);
          EndScreen.this.returnToMenu();
        }
        
        super.touchUp(event, x, y, pointer, button);
      }
    });
    this.stage.addActor(returnButton);
    
    TextButton submitScore = new TextButton("Submit Highscore ", style);
    submitScore.setSize(256.0F, 96.0F);
    submitScore.setPosition(Gdx.graphics.getWidth() / 2 - 128, 
      Gdx.graphics.getHeight() / 2 - 356);
    submitScore.addListener(new InputListener()
    {
      public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        return true;
      }
      
      public void touchUp(InputEvent event, float x, float y, int pointer, int button)
      {
        if (button == 0) {
          Resources.GUISOUNDS[0].play();
          EndScreen.this.state = 1;
        }
      }
    });
  }
  






  public static boolean isServerUp()
  {
    try
    {
      URL url = new URL("http://biomes.host-ed.me/");
      URLConnection con = url.openConnection();
      con.connect();
      return true;
    } catch (Exception e) {
      e.printStackTrace(); }
    return false;
  }
  

  private void returnToMenu()
  {
    if (KnightsAndArrows.isOffline()) {
      KnightsAndArrows.INSTANCE.setScreen(new MenuScreen("Guest", false));
    } else
      KnightsAndArrows.INSTANCE.setScreen(new MenuScreen(SplashScreen.username, true));
  }
  
  public void show() {
    this.batch = new SpriteBatch();
    this.stage = new Stage();
    this.stage2 = new Stage();
  }
  
  public void hide() {
    dispose();
  }
  

  public void pause() {}
  
  public void resume() {}
  
  public void dispose()
  {
    this.stage.dispose();
    this.stage2.dispose();
    this.batch.dispose();
  }
}
